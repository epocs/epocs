@echo off
for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set DATETIMESTAMP=%%j
set "YYYYMMDD=%DATETIMESTAMP:~0,8%
set   "HHMMSS=%DATETIMESTAMP:~8,6%
set "GITLOGFILE=%~dp0system-logs\%YYYYMMDD%-%HHMMSS%-update.txt
mkdir system-logs 2>NUL
set "GITURL=https://git-scm.com/download/win
where git 2>NUL>NUL || echo To enable automatic updates, you will need to install git. ^
                    && echo. ^
                    && echo If you like, I'll take you to %GITURL% now^
                    && echo. ^
                    && pause ^
                    && start %GITURL% ^
                    && goto :eof
@echo on

@echo "%GITLOGFILE%" > "%GITLOGFILE%
git pull %* 2>&1 >> "%GITLOGFILE%
@type "%GITLOGFILE%
:: TODO: would be nice to have a copy of the pull log on file too but Windows has no tee utility
@echo.>>"%GITLOGFILE%
@git log --graph --date-order --date=iso-local --abbrev-commit --decorate=short --all --max-count=4 >> "%GITLOGFILE%
@notepad "%GITLOGFILE%
::@pause
