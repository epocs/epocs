set parameter VisualizeTiming        1
set parameter VisualizeSource        1
set parameter VisualizeSpatialFilter 1
set parameter VisualizeIIRBandpass   1
set parameter VisualizeTrapFilter    1
#set parameter RangeIntegrator        1  # visualization seems to have gone haywire with this one in recent BCI2000 versions