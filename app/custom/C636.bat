

SET PARAMETER     Source        int        SourceCh=                   9
SET PARAMETER     Source        floatlist  SourceChOffset=             9     0         0         0         0         0         0         0         0         0         0     
SET PARAMETER     Source        floatlist  SourceChGain=               9   162.07    162.07    162.07    162.07    162.07    162.07    162.07    162.07    162.07    162.07   // TODO: NB: this will have to be calibrated for each setup. Also calibration not yet verified for channels that go through the AMT-8 amp
SET PARAMETER     Source        list       ChannelNames=               9   EMG1      EMG2      TRIG      EMG3      EMG4      EMG5      EMG6      EMG7      EMG8  TMSTRIG      // 

SET PARAMETER  SpatialFilterType  0

SET PARAMETER     Trigger       list       ChannelsToTrap=             2   EMG1      EMG2    // names or indices of the input channels to be trapped

SET PARAMETER  VisualizeSource    1

