set parameter  "Source matrix OutputSignals= 2 { Analog%20or%20Digital DAQmx%20Channel%20Spec Expression AnalogMin AnalogMax } Digital port0/Line7 EnableTrigger % %      Digital port0/Line5  NewResult&&ResponseGreen -1.0 1.0 "

SET PARAMETER     Source        int        SourceCh=                   6
SET PARAMETER     Source        floatlist  SourceChOffset=             6     0         0         0         0         0         0         0         0     
SET PARAMETER     Source        floatlist  SourceChGain=               6   162.07    162.07    162.07    162.07    162.07    162.07    162.07    162.07   // TODO: NB: this will have to be calibrated for each setup. Also calibration not yet verified for channels that go through the AMT-8 amp
SET PARAMETER     Source        list       ChannelNames=               6   EMG1      EMG2      TRIG      RESP      EKG       TRIGVNS   X1        X2       // 
SET PARAMETER     Trigger       list       ChannelsToTrap=             2   EMG1      EMG2      TRIG      RESP      EKG       TRIGVNS   X1        X2       // names or indices of the input channels to be trapped


set parameter LookBack     100ms
set parameter LookForward  500ms

set parameter VisualizeSource        1
set parameter VisualizeTrapFilter    1
