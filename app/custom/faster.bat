# # Default base settings:
# #   SamplingRate is 3200
# #   SampleBlockSize is 128
# #   => msecPerBlock is 40
# #  BackgroundSegmentDuration is 200ms

# # The following parameters will be overridden by GUI settings anyway, no matter what we specify here,
# # so they are provided just for documentation purposes.  But if there is a corresponding .ini setting,
# # we can use that, in some --defaults=WHATEVER.ini file, to set the GUI defaults for new subjects.
#set parameter Background    matrix     BackgroundChannels=         2 { Input%20Channel Subtract%20Mean? Norm Min%20Amplitude Max%20Amplitude Feedback%20Weight }                                                               EMG1              yes         1        0mV           %               1.0             EMG2              yes         1        %           %               0.0          // specification of signal channels used to monitor baseline compliance
#
#set parameter MinTimeBetweenTriggers       400ms   # corresponds to epocs setting in .ini file:  { '_SecondsBetweenTriggers': 0.4, }
#                                                   # NB: the GUI will not allow anything below 2s unless you
#                                                   # launch with some --defaults=custom.ini where custom.ini
#                                                   # contains (say) { '_MinimumSecondsBetweenTriggers': 0.1, }
#                                                   # (note the misleading similarity between the name of that
#                                                   # secondary epocs setting and the name of the BCI2000
#                                                   # parameter: they play different roles).
#set parameter MaxRandomTimeBetweenTriggers 200ms   # corresponds to epocs setting in .ini file:   { '_RandSecondsBetweenTriggers' : 0.2, }
#
#set parameter BackgroundHoldDuration       200ms   # corresponds to epocs setting in .ini file:  { '_BackgroundHoldSec' : 0.2, }
# # Note that you cannot rely on hold duration to enforce a minimum interval between trials, because the hold detector is free to look back across more than one trial.
# # Nonetheless this needs to be low because the stimulation artifact will typically cause the background EMG to violate its hold condition.


# # The following parameters have no GUI fields, but are still overridden by settings that have a different name
# set parameter MaxRandomExtraHoldDuration     0ms   # corresponds to epocs setting in .ini file: { '_BackgroundHoldExtraSec' : 0, } which defaults to 0 in epocs/__init__.py anyway


# # The following parameters can be configured here
set parameter LookForward                  100ms   # usually this is 500ms but if it is set greater than MinTimeBetweenTriggers then the TrapFilter will miss some trials
set parameter BackgroundFreezeTime         200ms

set parameter VisualizeRangeIntegrator       0     # this visualizer is broken and annoying
