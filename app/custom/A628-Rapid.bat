SET PARAMETER LookForward                  100ms   # usually this is 500ms but if it is set greater than MinTimeBetweenTriggers then the TrapFilter will miss some trials
SET PARAMETER BackgroundFreezeTime         200ms

SET PARAMETER VisualizeRangeIntegrator       0     # this visualizer is broken and annoying



SET PARAMETER     Source        int        SourceCh=                   5
SET PARAMETER     Source        floatlist  SourceChOffset=             5     0         0         0         0         0     
SET PARAMETER     Source        floatlist  SourceChGain=               5   162.07    162.07    162.07    162.07    162.07   // TODO: NB: this will have to be calibrated for each setup. Also calibration not yet verified for channels that go through the AMT-8 amp
SET PARAMETER     Source        list       ChannelNames=               5   EMG1      EMG2      TRIG      EMG3      EMG4     // 
SET PARAMETER     Trigger       list       ChannelsToTrap=             2   EMG1      EMG2      TRIG      EMG3      EMG4     // names or indices of the input channels to be trapped
