# this is the place to experiment with gradient transformations and corrections for grid slippage

set parameter SpatialFilterType  2      # 0 to disable, 2 to turn on sparse spatial filter. Don't touch other modes.

# Michael's first zero-sum gradient transformation (order 4,4,2,  center 0,0, angle 0)
# As a base, use just the first 33 rows (30 weights contributing to EMG1, 2 weights (bipolar) for EMG2, and a passthrough for TRIG) :
# If you need all channels additionally passed through (for some future live/interactive on-the-fly spatial filtering implementation) then use all 65 rows:
#set parameter Filtering matrix   SpatialFilter=    33 { In Out Wt } acq01 EMG1 -6.1661 acq02 EMG1 -1.3819 acq03 EMG1 5.5809 acq04 EMG1 9.4184 acq05 EMG1 1.2970 acq06 EMG1 -2.8804 acq07 EMG1 5.9757 acq08 EMG1 11.1802 acq09 EMG1 9.2093 acq10 EMG1 -3.4608 acq11 EMG1 -1.0059 acq12 EMG1 2.4298 acq13 EMG1 4.6483 acq14 EMG1 4.4749 acq15 EMG1 0.7353 acq16 EMG1 1.0059 acq17 EMG1 -2.4298 acq18 EMG1 -4.6483 acq19 EMG1 -4.4749 acq20 EMG1 -0.7353 acq21 EMG1 2.8804 acq22 EMG1 -5.9757 acq23 EMG1 -11.1802 acq24 EMG1 -9.2093 acq25 EMG1 3.4608 acq26 EMG1 6.1661 acq27 EMG1 1.3819 acq28 EMG1 -5.5809 acq29 EMG1 -9.4184 acq30 EMG1 -1.2970 acq31 EMG2 33.3333 acq32 EMG2 -33.3333 TRIG TRIG 1.0 acq01 acq01 1.0   acq02 acq02 1.0   acq03 acq03 1.0   acq04 acq04 1.0   acq05 acq05 1.0   acq06 acq06 1.0   acq07 acq07 1.0   acq08 acq08 1.0   acq09 acq09 1.0   acq10 acq10 1.0   acq11 acq11 1.0   acq12 acq12 1.0   acq13 acq13 1.0   acq14 acq14 1.0   acq15 acq15 1.0   acq16 acq16 1.0   acq17 acq17 1.0   acq18 acq18 1.0   acq19 acq19 1.0   acq#20 acq20 1.0   acq21 acq21 1.0   acq22 acq22 1.0   acq23 acq23 1.0   acq24 acq24 1.0   acq25 acq25 1.0   acq26 acq26 1.0   acq27 acq27 1.0   acq28 acq28 1.0   acq29 acq29 1.0   acq30 acq30 1.0   acq31 acq31 1.0   acq32 acq32 1.0

# uncomment the line below if the 30-channel octagon grid is not connected (only the two contacts that contribute to the bipolar channel will contribute to bias estimation)
#set parameter Source    intlist  ExcludeFromBias=  30  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30   // one-based indices (according to device's numbering) of channels to exclude from computation of bias for common-mode cancellation

# Weights for the soleusTA patch (again, use either all 65 or just the first 33)
set parameter Filtering matrix SpatialFilter=  33 { In Out Wt } acq01 EMG1 -5.9004 acq02 EMG1 0.2220 acq03 EMG1 2.2628 acq04 EMG1 0.2220 acq05 EMG1 -5.9004 acq06 EMG1 5.8116 acq07 EMG1 9.4851 acq08 EMG1 10.7096 acq09 EMG1 9.4851 acq10 EMG1 5.8116 acq11 EMG1 3.4958 acq12 EMG1 4.7203 acq13 EMG1 5.1285 acq14 EMG1 4.7203 acq15 EMG1 3.4958 acq16 EMG1 -3.4958 acq17 EMG1 -4.7203 acq18 EMG1 -5.1285 acq19 EMG1 -4.7203 acq20 EMG1 -3.4958 acq21 EMG1 -5.8116 acq22 EMG1 -9.4851 acq23 EMG1 -10.7096 acq24 EMG1 -9.4851 acq25 EMG1 -5.8116 acq26 EMG1 5.9004 acq27 EMG1 -0.2220 acq28 EMG1 -2.2628 acq29 EMG1 -0.2220 acq30 EMG1 5.9004 acq31 EMG2 33.3333 acq32 EMG2 -33.3333 TRIG TRIG 1.0 acq01 acq01 1.0   acq02 acq02 1.0   acq03 acq03 1.0   acq04 acq04 1.0   acq05 acq05 1.0   acq06 acq06 1.0   acq07 acq07 1.0   acq08 acq08 1.0   acq09 acq09 1.0   acq10 acq10 1.0   acq11 acq11 1.0   acq12 acq12 1.0   acq13 acq13 1.0   acq14 acq14 1.0   acq15 acq15 1.0   acq16 acq16 1.0   acq17 acq17 1.0   acq18 acq18 1.0   acq19 acq19 1.0   acq20 acq20 1.0   acq21 acq21 1.0   acq22 acq22 1.0   acq23 acq23 1.0   acq24 acq24 1.0   acq25 acq25 1.0   acq26 acq26 1.0   acq27 acq27 1.0   acq28 acq28 1.0   acq29 acq29 1.0   acq30 acq30 1.0   acq31 acq31 1.0   acq32 acq32 1.0

# For live/interactive spatial-filtering, you'll also need to trap them all - uncomment this:
#set parameter Trigger   list     ChannelsToTrap=   34  EMG1  EMG2 acq01 acq02 acq03 acq04 acq05 acq06 acq07 acq08 acq09 acq10 acq11 acq12 acq13 acq14 acq15 acq16 acq17 acq18 acq19 acq20 acq21 acq22 acq23 acq24 acq25 acq26 acq27 acq28 acq29 acq30 acq31 acq32    // names or indices of the input channels to be trapped

set parameter VisualizeTiming 1
set parameter VisualizeSource 1
set parameter VisualizeSpatialFilter 1
set parameter VisualizeIIRBandpass 1
set parameter VisualizeTrapFilter 1
