#!../prog/BCI2000Shell
@cls & ..\prog\BCI2000Shell %0 %* #! && exit /b 0 || exit /b 1

system taskkill /F /FI "IMAGENAME eq NIDAQ_mx_Source.exe"
system taskkill /F /FI "IMAGENAME eq DSISerial.exe"
system taskkill /F /FI "IMAGENAME eq BiocircuitPNISource.exe"
system taskkill /F /FI "IMAGENAME eq gUSBampSource.exe"
system taskkill /F /FI "IMAGENAME eq FilePlayback.exe"
system taskkill /F /FI "IMAGENAME eq ReflexConditioningSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq aReflexConditioningSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"

system taskkill /F /FI "IMAGENAME eq NIDAQ_~1.exe"
system taskkill /F /FI "IMAGENAME eq DSISER~1.exe"
system taskkill /F /FI "IMAGENAME eq BIOCIR~1.exe"
system taskkill /F /FI "IMAGENAME eq GUSBAM~1.exe"
system taskkill /F /FI "IMAGENAME eq FILEPL~1.exe"
system taskkill /F /FI "IMAGENAME eq REFLEX~1.exe"
system taskkill /F /FI "IMAGENAME eq AREFLE~1.exe"
system taskkill /F /FI "IMAGENAME eq DUMMYA~1.exe"

change directory $BCI2000LAUNCHDIR

set environment MODE   $1   # "standalone" or "skinned"
set environment SOURCE $2   # "nidaqmx" or "gusbamp" or "pni" or a filename for replay 
set environment CUSTOM $3   # empty, or a path to a BCI2000 script file

if [ $MODE   == "" ]; set environment MODE   "standalone"; end
if [ $SOURCE == "" ]; set environment SOURCE "nidaqmx";    end

if [ $MODE == "standalone" ]; show window; end
set title ${extract file base $0}
reset system

if [ $EPOCSTIMESTAMP == "" ]; set environment EPOCSTIMESTAMP $YYYYMMDD-$HHMMSS; end
startup system localhost --SystemLogFile=../../system-logs/$EPOCSTIMESTAMP-operator.txt

if [ $SOURCE == "nidaqmx" ]
	start executable NIDAQ_mx_Source     --local
elseif [ $SOURCE == "gusbamp" ]
	start executable gUSBampSource       --local
elseif [ $SOURCE == "pni" ]
	start executable BiocircuitPNISource --local
elseif [ $SOURCE == "dsi" ]
	if [ ${get environment EPOCS_TTLWIDGET} == "" ]
		error when using --source=dsi, environment variable EPOCS_TTLWIDGET must be set, to allow access to a pulse-generating microcontroller 
	elseif [ ${get environment EPOCS_TTLWIDGET} == "DUMMY" ]
		set environment EPOCS_TTLWIDGET ""
	end
	start executable DSISerial           --local --SerialPort=${get environment EPOCS_TTLWIDGET} --PublishCommand=publish\n
elseif [ $SOURCE == "signalgenerator" ]
	if [ ${get environment EPOCS_TTLWIDGET} == "" ]
		error when using --source=signalgenerator, environment variable EPOCS_TTLWIDGET must be set, to allow access to a pulse-generating microcontroller 
	end
	start executable SignalGenerator     --local --SerialPort=${get environment EPOCS_TTLWIDGET} --PublishCommand=publish\n
elseif [ ${is file $SOURCE} ]
	start executable FilePlayback        --local --EvaluateTiming=0 --PlaybackFileName=$SOURCE
	set environment SOURCE "replay"
else
	error unable to get data from source "$SOURCE"
end
start executable DummyApplication                   --local
if [ $SOURCE != "replay" ]; sleep 1; end
start executable ReflexConditioningSignalProcessing --local --NumberOfThreads=1

add parameter Storage:Session                        string   SessionStamp=           %     %  % % // 
add parameter Application:Operant%20Conditioning     string   EPOCSRevision=          %     %  % % // 
add parameter Application:Operant%20Conditioning     string   ApplicationMode=       ST     %  % % // 
add parameter Application:Operant%20Conditioning     float    BackgroundScaleLimit=  20mV 20mV 0 % // 
add parameter Application:Operant%20Conditioning     float    ResponseScaleLimit=    20mV 20mV 0 % // 
add parameter Application:Operant%20Conditioning     float    BaselineResponseLevel=  %    8mV 0 % //
add parameter Application:Operant%20Conditioning     float    MwaveTarget=           30mV 30mV % % //
add parameter Application:Operant%20Conditioning     float    MwavePercentage=       20   20   % % //
add parameter Application:Operant%20Conditioning     float    TargetPercentile=      66   66   % % //

wait for connected

if [ $SOURCE == "replay" ]
	# do nothing - just use the parameters from the file (vital for SamplingRate and SourceCh*, and desirable for SampleBlockSize - but note that epocs.py may overrule many parameters)
	
	if [ ${is parameter EnableDS5ControlFilter} ]; set parameter EnableDS5ControlFilter 0; end #  stop trying to control the hardware during replay
	if [ ${is parameter EnableDS8ControlFilter} ]; set parameter EnableDS8ControlFilter 0; end # 
	if [ ${is parameter AnalogOutput}  ];          set parameter AnalogOutput           0; end # (this one has to be 0 anyway, as analog output is deprecated)
	set parameter AlignChannels 0  # NB: this is an overgeneralized asssumption (it's possible that the file was recorded with an old-fashioned amp without sample-and-hold) but it's more likely that you have a spurious AlignChannels=1 in the .dat file, as with all files recorded before 2022-03-18
else
	load parameterfile ../parms/base-nidaqmx.prm  # nidaqmx is the base from which all the others diverge
	
	if [ $SOURCE == "nidaqmx" ]
		load parameterfile ../parms/NIDigitalOutputPort.prm
		load parameterfile ../parms/base-nidaqmx.prm	# TODO: ideally we should delete this line, or swap its order relative to the line above. Right now it overwrites the site-specific NIDigitalOutputPort.prm (which has presumably-more-up-to-date SourceChGain values) and so makes NIDigitalOutputPort.prm useless. However, the implementation has always done it this way (bugged?) so we're being cautious about making the change where MUSC's setups are concerned.
	elseif [ $SOURCE == "gusbamp" ]
		load parameterfile ../parms/base-gUSBamp.prm
	elseif [ $SOURCE == "pni" ]
		load parameterfile ../parms/base-BiocircuitPNI.prm
	elseif [ $SOURCE == "dsi" ]
		load parameterfile ../parms/base-DSI.prm
	elseif [ $SOURCE == "signalgenerator" ]
		#load parameterfile ../parms/base-DSI.prm
		set parameter TTLInputPin  27 # Ortiz box BNC pin
		set parameter TTLOutputPin 30 # Ortiz box BNC pin
		set parameter TriggerState TTLInput
		set parameter Source        matrix     SerialOutputs=       2 2   EnableTrigger>0 output=1\n   EnableTrigger==0  output=0\n    // matrix of expressions for Digital Output
	end
end
if [ $MODE == "standalone" ]
	set parameter VisualizeTiming 1
	set parameter VisualizeSource 1
	set parameter VisualizeSpatialFilter 1
	set parameter VisualizeIIRBandpass 1
	set parameter VisualizeBackgroundAverages 1
	set parameter VisualizeTrapFilter 1
	set parameter VisualizeRangeIntegrator 1
else
	set parameter OutputMode 0
end

if [ $CUSTOM ]
	execute script $CUSTOM
end

if [ $MODE == "standalone" ]
	setconfig
	set state Running 1
end
