#!/usr/bin/env -S python #
""" " 2>NUL
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
setlocal
set "CUSTOMPYTHONHOME=%PYTHONHOME_EPOCS%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*
endlocal
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

__doc__ = r"""
{execName} [-n] [BCI2000ROOT] [RESTRICT_TO_FILENAMES...]

For any file that's git-tracked inside `app/prog` here, see if there
is a corresponding file in the `prog` directory of `BCI2000ROOT`. If
there is, copy it here.  Likewise for `app/tools/cmdline`.

-n:    Dry run: print what would be done, but don't do it.
-s:    Run `SignBinaries.cmd` on any copied `.dll` and `.exe` files

The default `BCI2000ROOT` is `../../../bci2000-svn/HEAD` relative
to the location of this script.

Optional further arguments (`RESTRICT_TO_FILENAMES`) are the
basenames of files that you want to include in the copy operation.
Filenames can contain `?` and `*` wildcards. Arguments that begin
with `!` are considered exclusion rather than inclusion patterns.
If you do not specify any inclusion patterns, all possible files are
considered.

Examples::

	{execName} \neurotech\bci2000-svn\HEAD  !BCI2000Remote* -s -n
	{execName} \neurotech\bci2000-svn\HEAD  *.py -n

"""

DEFAULT_SRCROOT = '../../bci2000-svn/HEAD' # relative to the destination BCI2000 distro (dstRoot)

import os
import sys
import glob
import shlex
import shutil
import fnmatch
import subprocess

if sys.version < '3': bytes = str
else: unicode = str; basestring = ( unicode, bytes )
def IfStringThenRawString( x ):
	return x.encode( 'utf-8' ) if isinstance( x, unicode ) else x
def IfStringThenNormalString( x ):
	if str is bytes or not isinstance( x, bytes ): return x
	try: return x.decode( 'utf-8' )
	except: pass
	try: return x.decode( sys.getfilesystemencoding() )
	except: pass
	return x.decode( 'latin1' ) # bytes \x00 to \xff map to characters \x00 to \xff (so, in theory, cannot fail)

def Bang( cmd, shell=False, stdin=None, cwd=None, raiseException=False ):
	windows = sys.platform.lower().startswith('win')
	# If shell is False, we have to split cmd into a list---otherwise the entirety of the string
	# will be assumed to be the name of the binary. By contrast, if shell is True, we HAVE to pass it
	# as all one string---in a massive violation of the principle of least surprise, subsequent list
	# items would be passed as flags to the shell executable, not to the targeted executable.
	# Note: Windows seems to need shell=True otherwise it doesn't find even basic things like ``dir``
	# On other platforms it might be best to pass shell=False due to security issues, but note that
	# you lose things like ~ and * expansion
	if isinstance( cmd, str ) and not shell:
		if windows: cmd = cmd.replace( '\\', '\\\\' ) # otherwise shlex.split will decode/eat backslashes that might be important as file separators
		cmd = shlex.split( cmd ) # shlex.split copes with quoted substrings that may contain whitespace
	elif isinstance( cmd, ( tuple, list ) ) and shell:
		quote = '"' if windows else "'"
		cmd = ' '.join( ( quote + item + quote if ' ' in item else item ) for item in cmd )
	try: sp = subprocess.Popen( cmd, shell=shell, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
	except OSError as exc: returnCode, output, error = 'command failed to launch', '', str( exc )
	else: output, error = [ IfStringThenNormalString( x ).strip() for x in sp.communicate( stdin ) ]; returnCode = sp.returncode
	if raiseException and returnCode:
		if isinstance( returnCode, int ): returnCode = 'command failed with return code %s' % returnCode
		raise OSError( '%s:\n    %s\n    %s' % ( returnCode, cmd, error ) )
	return returnCode, output, error


def WindowsPath( x, short=False ):
	# http://stackoverflow.com/a/23598461/200291
	x = os.path.realpath( x )
	import ctypes
	from ctypes import wintypes
	func = ctypes.windll.kernel32.GetShortPathNameW if short else ctypes.windll.kernel32.GetLongPathNameW
	func.argtypes = [ wintypes.LPCWSTR, wintypes.LPWSTR, wintypes.DWORD ]
	func.restype  = wintypes.DWORD
	output_buf_size = 0
	while True:
		output_buf = ctypes.create_unicode_buffer( output_buf_size )
		needed = func( x, output_buf, output_buf_size )
		if output_buf_size >= needed: return output_buf.value
		else: output_buf_size = needed

def Copy( src, dst, dry=False ):
	if os.path.isdir( src ):
		verb = 'would create' if dry else 'creating'
		if not os.path.isdir( dst ):
			n = max( len( src ), len( dst ) )
			if dry: print( '%12s %*s' % ( verb, n, dst ) )
			else: os.mkdir( dst )
		for child in sorted( glob.glob( os.path.join( src, '*' ) ) ):
			Copy( child, os.path.join( dst, child[ len( src ): ].lstrip( '\\/' ) ), dry=dry )
	else:
		verb = 'would copy' if dry else 'copying'
		n = max( len( src ), len( dst ) )
		print( '\n%12s %*s\n%12s %*s' % ( verb, n, src, 'to', n, dst ) )
		if not dry: shutil.copy2( src, dst )

if __name__ == '__main__':
	startDir = os.getcwd()
	try:
		dstRoot = os.path.realpath( os.path.join( __file__, '..', '..' ) )
		
		args = sys.argv[ 1: ]
		
		dry = False
		while '-n' in args: args.remove( '-n' ); dry = True
			
		sign = False
		while '-s' in args: args.remove( '-s' ); sign = True
			
		help = False
		while '-h' in args: args.remove( '-h' ); help = True
		if help: raise SystemExit( __doc__.format( execName=os.path.basename( sys.argv[ 0 ] ) ) )
			
		srcRoot = args.pop( 0 ) if args and '*' not in args[ 0 ] else ''
		if not srcRoot: srcRoot = os.path.join( dstRoot, DEFAULT_SRCROOT )
		srcRoot = os.path.realpath( os.path.expanduser( srcRoot ) )
		if os.path.split( srcRoot )[ -1 ] == 'prog': srcRoot = os.path.dirname( srcRoot )
		
		
		NOT = '!'
		restrictToFileNames = [ arg                for arg in args if not arg.startswith( NOT ) ] #
		excludeFileNames    = [ arg[ len( NOT ): ] for arg in args if     arg.startswith( NOT ) ]
		
		sign = [ os.path.join( dstRoot, 'build', 'SignBinaries.cmd' ) ] if sign else []
		dependencyTool = os.path.join( srcRoot, 'build', 'buildutils', 'list_dependencies.exe' )
		os.chdir( dstRoot )
		returnCode, out, err = Bang( [ 'git', 'ls-files' ], raiseException=True )
		allFiles = out.split( '\n' )
		
		def matches( file, *patterns ):
			fdir, fbase = os.path.split( file.replace( '\\', '/' ) )
			for pattern in patterns:
				pdir, pbase = os.path.split( pattern.replace( '\\', '/' ) )
				match = fnmatch.fnmatch( fbase, pbase )
				if pdir: match &= fnmatch.fnmatch( fdir, pdir )
				if match: return True
			return False
			
		for pattern in [ 'prog/*', 'tools/cmdline/*', 'tools/python/BCI2000Tools/*' ]:
			partial = os.path.dirname( pattern )
			srcDir = os.path.join( srcRoot, partial )
			dstDir = os.path.realpath( os.path.join( dstRoot, partial ) )
			if not os.path.isdir( dstDir ): print( '\nNo directory %s\n' % dstDir ); continue
			x = 'Version-controlled files in %s:' % dstDir; print( '\n%s\n%s\n' % ( x, '=' * len( x ) ) )
			
			dependencies = {}
			qtplugins = os.path.join( srcDir, 'qtplugins' ) # special case: modules have the (deep-tree) contents of this directory as a dependency, but they don't know that they do.
			srcFiles, dstFiles = [], []
			for file in allFiles:
				if not matches( file, pattern ): continue
				#print(file)
				if restrictToFileNames and not matches( file, *restrictToFileNames ): continue
				if excludeFileNames    and     matches( file, *excludeFileNames ):
					print( 'skipping due to exclusion pattern: {}\n'.format( file ) )
					continue
				srcFiles.append( os.path.join( srcRoot, file.replace( '/', os.path.sep ) ) )
				dstFiles.append( os.path.join( dstRoot, file.replace( '/', os.path.sep ) ) )
			for srcFile, dstFile in zip( srcFiles, dstFiles ):
				if os.path.isfile( srcFile ):
					Copy( srcFile, dstFile, dry=dry )
					if sign and dstFile.lower().endswith( ( '.exe', '.dll' ) ):
						sign.append( dstFile ) # TODO: we don't want to sign the third-party DLL dependencies, do we? 
					if srcFile.lower().endswith( ( '.dll', '.exe' ) ):
						for redistFlag in [ '--redist', '' ]:
							returnCode, out, err = Bang( [ dependencyTool, redistFlag, srcFile ] if redistFlag else [ dependencyTool, srcFile ], raiseException=False )
							if returnCode: print( '!failed:    %s %s "%s"\n            %s' % ( os.path.basename( dependencyTool ), redistFlag, srcFile, err ) ); out = ''
							for dll in out.replace( '\r\n', '\n' ).split( '\n' ):
								if not dll.strip(): continue
								dll = WindowsPath( dll )
								if dll in srcFiles: continue
								if not redistFlag and not dll.lower().startswith( srcRoot.lower() ): continue
								dllkey = os.path.basename( dll ).lower()
								dependencies[ dllkey ] = dll
								if dllkey.startswith( 'qt' ) and os.path.isdir( qtplugins ):
									dependencies[ 'qtplugins' ] = qtplugins
									
			if dependencies: x = 'Outstanding dependencies for %s:' % dstDir; print( '\n%s\n%s\n' % ( x, '=' * len( x ) ) )
			# After the first time this is used, the dependencies themselves will presumably go into version control,
			# so next time they will be copied as principal files (above), rather than as "outstanding dependencies"
			# (below). If the BCI2000 distro changes its dependencies, the old ones will no longer exist in src and
			# so will be omitted from copy, and the new ones will be copied as "outstanding". This will be noticeable
			# in the git status, which will act as a reminder to remove the old ones.
			for dllkey, srcItem in sorted( dependencies.items() ):
				Copy( srcItem, os.path.join( dstDir, os.path.basename( srcItem ) ), dry=dry )
				
		if len( sign ) > 1:
			os.chdir( os.path.join( dstRoot, 'build' ) )
			sign = [ os.path.basename( sign[ 0 ] ) ] + [ os.path.relpath( x, os.getcwd() ) for x in sign[ 1: ] ] # because signtool.exe seems to have trouble/weird behaviour with path resolution
			
			verb = 'would then sign' if dry else 'signing'
			print( '{} with:\n\n    cd /D "{}"\n    {}\n\n'.format( verb, os.getcwd(), ' '.join( '"' + x + '"' for x in sign ) ) )
			if not dry:
				returnCode, out, error = Bang( sign )
				print( error if returnCode or error else out )
	finally:
		os.chdir( startDir )