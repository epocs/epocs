:: The hope is that signing binaries (especially certain members of
:: `tools\cmdline\*.exe` for some reason) will reduce the likelihood of
:: certain antivirus suites (e.g. FireEye and CrowdStrike) quarantining
:: them at runtime.
::
:: TODO: currently this script assumes that you have only one `.pfx`
:: file installed on your system, and it will sign with that.
:: 
:: Usage: first run `VisualStudio.cmd` to ensure that signtool.exe is
:: on your `%PATH%`. Then::
:: 
::     SignBinaries ..\prog\*.exe ..\prog\*.dll ..\tools\cmdline\*.exe
::
:: NB: signtool.exe doesn't seem to accept absolute paths and/or has
::     some very weird automatic path resolution mechanism. Work from
::     the directory containing this script, and use relative paths...

@echo.
@where signtool.exe 2>NUL || echo Could not find the path to signtool.exe - maybe initialize VisualStudio paths? && goto :eof
@echo.
@for /R %%f in (%*) do @(
	signtool.exe sign /a /t http://timestamp.comodoca.com %%f
	signtool.exe sign /as /fd sha256 /tr http://timestamp.comodoca.com?td=sha256 /td sha256 %%f
) 
@echo.
