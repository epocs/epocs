#!/usr/bin/env -S python #
""" " 2>NUL
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
setlocal
set "CUSTOMPYTHONHOME=%PYTHONHOME_EPOCS%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*
endlocal
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

#############################################################

__doc__ = r"""
Automatically export raw signals (default) or trial-by-trial summaries
based on offline analysis of one or more EPOCS .dat files.

Note: if epocs/data/$SUBJECTNAME/$SUBJECTNAME-LastSettings-Offline.txt exists,
it will be loaded and the timings used from there.  As a fallback, the timings
will come from the .dat file.
"""

if __name__ == '__main__':
	import argparse	
	parser = argparse.ArgumentParser( description=__doc__, formatter_class=argparse.RawTextHelpFormatter )
	parser.add_argument(         "filenames",    metavar='DATFILE', nargs='*', help='BCI2000 data file(s) to load. Each filename may optionally have a colon-delimited suffix specifying the maximum number of trials to load.' )
	parser.add_argument( "-s", "--summary",      action='store_true', help='export trial-by-trial summary data instead of raw signals' )
	parser.add_argument( "-o", "--output",       metavar='OUTPUT_CSV_FILENAME', help='output filename (leave blank to use GUI)' )
	parser.add_argument( "-c", "--channel",      metavar='CHANNEL',   help='channel name (leave blank for default)' )
	parser.add_argument( "-p", "--custom",       metavar='PRMFILE',   help='parameter string, or name of parameter file, to customize the offline analysis' )
	parser.add_argument( "-t", "--save-timings", action='store_true', help='if true, save the timings and other settings in epocs/data/$SUBJECTNAME/$SUBJECTNAME-LastSettings-Offline.txt' )
	
	opts = parser.parse_args()

############################################################

__all__ = [
	'AutoExport',
]

import os
import sys
import epocs

def AutoExport( filenames, output=None, channel=None, summary=False, close=True, save_timings=False, **kwargs ):
	"""
	Example:: 

		AutoExport(
			[
				'C:/neurotech/epocs/data/sample/default-replay.dat:8',  # the :8 at the end means "only the first 8 trials"
				'C:/neurotech/epocs/data/sample/default-replay.dat:12', # process more than one file in sequence (or hey, the same file twice) if you want
			],
			
			# maxTrials=40,   # a different way of limiting the number of trials for each file (may be a list; `None` means no limit)
			# duration='15s', # that's another (older) way of limiting the amount of signal processed (global for all files)
			
			# summary=True,   # uncomment this if you want to export summary data instead of raw signals
			# close=False,    # uncomment this if you want to keep the GUI open after exporting
		)
	
	Any `**kwargs` are passed through to `epocs.OfflineAnalysis.OpenFiles()`
	"""
	if hasattr( filenames, 'split' ):
		if '\n' in filenames: filenames = [ filename.strip() for filename in filenames.split( '\n' ) if filename.strip() ]
		else: filenames = [ filenames ]
	filenames = [ os.path.realpath( os.path.expanduser( filename ) ) for filename in filenames ]  # because OpenFiles seems to potentially change the working directory between files

	if output and summary:
		raise ValueError( 'at the moment, trial-by-trial summaries can only be saved in the default location under the default names' )

	oa = epocs.OfflineAnalysis( gui=False )
	window = oa.OpenFiles( filenames=filenames, **kwargs ) # this method does its own globbing now, and more
	# window is now an AnalysisWindow instance
	if window is None:
		msg = 'no matching files found with trials'
		try: raise FileNotFoundError( msg )
		except NameError: raise IOError( msg )
	if summary:
		window.Export( 'sequence' )
		print( 'trial-by-trial summaries exported' )
	else:
		if channel:
			window.ChangeChannel( channel )
			window.UpdateResults()  # NB this does select and draw the right signals, but doesn't update the text in the drop-down menu
		confirmWithGUI = not output
		if save_timings:
			window.PersistTimings()
		if output is None:
			output = os.path.splitext( os.path.basename( filenames[ 0 ] ) )[ 0 ] + '-' + window.GetSelectedChannelName()
		exported = window.overlay.ExportRawSignals( output, confirmWithGUI=confirmWithGUI )
		if exported: print( window.GetSelectedChannelName() + ' exported' )
		else: print( 'cancelled' )
	if close:
		if save_timings: oa.CloseWindow( window ) # this saves epocs/data/$SUBJECTNAME/$SUBJECTNAME-LastSettings-Offline.txt
		else: window.destroy()                    # this does not

if __name__ == '__main__':
	try: AutoExport( **opts.__dict__ )
	except IOError as err: raise SystemExit( err ) 
