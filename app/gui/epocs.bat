@echo off
set "OLDDIR=%CD%
cd /D "%~dp0"

if "%DEBUG_EPOCS%" == "" ( goto :gui ) else ( goto :console )

:gui
call "%~dp0SelectPython.bat" "%~dp0interpreter;%~dp0interpreter\App;%PYTHONHOME_EPOCS%"   start pythonw -m epocs --log=../../system-logs/###-python.txt %*
goto :finish

:console
call "%~dp0SelectPython.bat" "%~dp0interpreter;%~dp0interpreter\App;%PYTHONHOME_EPOCS%"   python -m epocs  %*
goto :finish

:finish
cd /D "%OLDDIR%"
