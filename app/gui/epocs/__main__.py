# coding=utf-8
"""
This is a graphical user interface (GUI) implementation for the evoked potential operant
conditioning system (EPOCS). This Python 2.x code uses the built-in Tkinter toolkit for
organizing and rendering GUI elements, and the third-party matplotlib package for rendering
graphs and other custom graphics.  Under the hood, the EPOCS GUI calls BCI2000 binaries.
BCI2000 does most of the actual real-time processing and saves the data file, but this GUI
replaces the BCI2000 Operator window and config dialog by providing an interface for the
therapist/researcher to configure, start and stop runs; it also replaces the BCI2000
Application module by providing real-time biofeedback to the patient/subject; finally it
provides specialized offline analysis of the most recently collected data.

This file may be run as a standalone python file.  It may take the following optional
command-line arguments:

--custom=XXX : specify a path XXX to a custom batch file, relative to the current (gui or
               gui-bin) directory.  For example,  --custom=../custom/VisualizeSource.bat
               The specified file will be interpreted by the BCI2000 scripting engine
               and run when BCI2000 has launched (see the web documentation on "BCI2000
               operator module scripting"). The custom script can be used to add custom
               parameters and state variables, and set parameter values---for example,
               to open BCI2000 visualization windows and to increase the number of
               acquired channels.  The best place to add a --custom switch is in the
               "Target" line in the properties of the shortcut you use to launch EPOCS.

--offline    : start in "offline analysis" mode (perform analysis on old files) instead of
               the default "online" mode. In this case, the --custom argument (if supplied)
               is interpreted as a BCI2000 .prm file containing parameter values that will
               override the .dat file's parameters during offline processing through the
               BCI2000 filter chain.

--devel      : start in "development" mode:  use the BCI2000 FilePlayback module as source
               instead of the live data acquisition module, and include a "Load Demo Data"
               button in some modes so that the "Analysis" buttons can be pressed immediately
               without having to wait to gather data. Internally, the --devel flag causes
               GLOBALS.DEVEL to be set to True.   The application also starts in development
               mode automatically if no interface to the acquisition hardware API can be
               found at the time of launch.

--debug      : start in "debug" mode:  this is usually but not always used in conjunction
               with --devel.  It causes GLOBALS.DEBUG to be set to True.  This contingency
               may be used to trigger behavior that is useful in debugging but which might
               not normally occur (for example, it was useful in debugging a problem with
               the StickySpanSelector in the Voluntary Contraction analysis window, which
               would prevent zooming when its value was set to some values but not others:
               if GLOBALS.DEBUG: set_to_problematic_values()   )

Notes Amir Eftekhar May/June 2016 - Added functionality -
	M-wave window: A window that allows the user to see analysis of the M-Wave, including
	Rectified amplitude, the mean for that set of trials and also to display the mean.
	Where additions are made they are noted with ###AMIR + DESC
"""


"""
TODO

	caveats and gotchas:
		BackgroundTriggerFilter.cpp issue: assuming background is in range, time between triggers actually
		seems to come out to MinTimeBetweenTriggers + 1 sample block

	nice-to-haves:
		NIDAQmxADC: acquisition of floating-point raw data instead of integers

		make separate settings entry to govern maximum random extra hold duration?  (if so: remember to enforce its rounding to whole number of segments)

		offline analysis
			maybe override ResponseInterval from .dat file with ResponseInterval from -LastSettings-Offline.txt config file?
				(but not with the setting from the online one, if present)
			(semi-)automatic removal of individual trials? using new outlier-removal tab?
			button for saving figures as pdf?
"""


import os
import sys
import time
import getopt
import ctypes
import inspect
import subprocess

try:     import Tkinter as tkinter,       tkMessageBox as messagebox # Python 2
except ImportError: import tkinter, tkinter.messagebox as messagebox # Python 3
	
import matplotlib; matplotlib.use('tkagg')

if 'IPython' in sys.modules: [sys.modules.pop(k) for k in list(sys.modules) if k == 'epocs' or k.startswith( 'epocs.' )] # For running iteratively from the IPython prompt.
import epocs; from epocs import GLOBALS
# NB: although it might have been nicer to access GLOBALS via some kind of relative import, this does not play
#     nice with the flawed `%run -m` implementation of the FullMonty's old version of IPython, which doesn't 
#     accept command-line arguments. So you are forced to say `run epocs/__main__.py` instead of `run -m epocs`.
#     Then, this file does not know that it is in a package, and won't do relative imports.


# Wrangle command-line arguments

GLOBALS.PYTHONFILE = globals().get( '__file__', None )
if not GLOBALS.PYTHONFILE:
	try: frame = inspect.currentframe(); GLOBALS.PYTHONFILE = inspect.getfile( frame )
	finally: del frame  # https://docs.python.org/3/library/inspect.html#the-interpreter-stack
GLOBALS.PYTHONFILE = os.path.realpath( GLOBALS.PYTHONFILE )

GLOBALS.ARGS[ : ] = getattr( sys, 'argv', [] )[ 1: ]
try: import EpocsCommandLineArguments  # if present, this might say something like args = [ "--offline" ]
except ImportError: EpocsCommandLineArguments = None  # if absent, no biggy
else: GLOBALS.ARGS += EpocsCommandLineArguments.args  # this is a way of smuggling hard-coded command-line arguments into a py2exe binary
opts, args = getopt.getopt( GLOBALS.ARGS, '', [ 'log=', 'devel', 'debug', 'offline', 'custom=', 'source=', 'timetravel=', 'dumpdata=', 'defaults=' ] )
opts = dict( opts )
if EpocsCommandLineArguments: print( 'Command-line arguments %r imported from %s' % ( EpocsCommandLineArguments.args, EpocsCommandLineArguments.__file__ ) )
# Grok options

timetravel = opts.get( '--timetravel', '' ) # this evil little option is all about faking datestamps on screenshots and filenames. Move along, nothing to see.
if timetravel:
	truetime = time.time
	timetravel = time.mktime( time.strptime( timetravel.replace( '-', '' ).replace( ':', '' ).replace( ' ', '' ), '%Y%m%d%H%M' ) ) - truetime()
	time.time = lambda: truetime() + timetravel

log = opts.get( '--log', None )
os.environ[ 'EPOCSTIMESTAMP' ] = time.strftime( '%Y%m%d-%H%M%S' )
if log:
	log = log.replace( '###', os.environ[ 'EPOCSTIMESTAMP' ] )
	log = os.path.realpath( log )
	logDir = os.path.split( log )[ 0 ]
	if not os.path.isdir( logDir ): os.mkdir( logDir )
	try:               sys.stdout = sys.stderr = open( log, 'wt', 0 )
	except ValueError: sys.stdout = sys.stderr = open( log, 'wt' )    # later versions of Python don't allow unbuffered access to files in 't' mode

GLOBALS.DEVEL    = ( '--devel'   in opts ) # use FilePlayback as signal source, and load example data at launch for easy test of analysis window
	
GLOBALS.DEBUG    = ( '--debug'   in opts ) # print debug messages to the system log, and possibly perform other temporary "weird" debugging operations (independent of whether we're in FilePlayback mode)
	
GLOBALS.OFFLINE  = ( '--offline' in opts )
	
GLOBALS.DUMP     = opts.get( '--dumpdata', '' )

GLOBALS.DEFAULTS = opts.get( '--defaults', '' )

GLOBALS.CUSTOM   = opts.get( '--custom', '' )

GLOBALS.SOURCE   = opts.get( '--source', '' )
if not GLOBALS.SOURCE:  # default to nidaqmx...
	try: ctypes.windll.nicaiu
	except: GLOBALS.DEVEL = True    # ...but automatically pop into DEVEL mode (using FilePlayback instead of live signal recording) if no NIDAQmx interface is found on this computer
	else: GLOBALS.SOURCE = 'nidaqmx'
	if GLOBALS.DEVEL and not GLOBALS.OFFLINE: GLOBALS.SOURCE = '../../data/sample/default-replay.dat'
interpretSourceAsFilePattern = '\\' in GLOBALS.SOURCE or ':' in GLOBALS.SOURCE or '/' in GLOBALS.SOURCE or '*' in GLOBALS.SOURCE or GLOBALS.SOURCE.lower().endswith( '.dat' )
if not interpretSourceAsFilePattern: GLOBALS.SOURCE = GLOBALS.SOURCE.lower()

# Check whether the NI device configuration has been set up (NB: only checks for digitial mode, not analog; also, the file isn't used properly in the batch file anyway - needs straightening out)

niSetupParamFile = os.path.abspath( os.path.join( GLOBALS.GUIDIR, '../parms', 'NIDigitalOutputPort.prm' ) )
if not GLOBALS.OFFLINE and GLOBALS.SOURCE == 'nidaqmx' and not os.path.isfile( niSetupParamFile ):
	fName = os.path.abspath( os.path.join( GLOBALS.GUIDIR, '../NISetup/NIDigitalOutput.exe' ) )
	top = tkinter.Tk()
	top.withdraw()
	messagebox.showinfo('NI Device','National Instruments device not setup.\n Press OK to continue and setup your device.',parent=top)
	top.destroy()
	p = subprocess.Popen(fName,shell=True)
	p.wait()


# Actually Run EPOCS and open the GUI

try: tkinter.ALLWINDOWS
except: tkinter.ALLWINDOWS = []
while len( tkinter.ALLWINDOWS ):
	try: tkinter.ALLWINDOWS.pop( 0 ).destroy()
	except: pass

if GLOBALS.OFFLINE:
	from BCI2000Tools.Chain import BCI2000ChainError # epocs.__init__ will have manipulated sys.path to make this possible
	try:
		self = epocs.OfflineAnalysis( data=None )
		self.pythonLogFile = log
		self.systemLogFile = None
		window = self.OpenFiles( filenames=( GLOBALS.SOURCE if interpretSourceAsFilePattern else None ), custom=GLOBALS.CUSTOM )
		if window and not 'IPython' in sys.modules:
			def wingo( self ):
				self.update()
				try: self.after_idle( 20, wingo )
				except: raise SystemExit()
			#wingo( window ); time.sleep(5)
			epocs.OfflineAnalysis.GLOBALROOT.mainloop()
	except BCI2000ChainError as exc:
		raise SystemExit( exc )

else:
	self = epocs.OnlineGUI( source=GLOBALS.SOURCE, custom=GLOBALS.CUSTOM )
	self.pythonLogFile = log
	self.systemLogFile = os.path.realpath( os.path.join( GLOBALS.BCI2000LAUNCHDIR, '..', '..', 'system-logs', os.environ[ 'EPOCSTIMESTAMP' ] + '-operator.txt' ) )
	# TODO: the line above recreates logic in the master batch file. Would be better DRY
	#self.operator.remote.WindowVisible = 1
	if self.ready: self.Loop()

if self.pythonLogFile:
	# remove the python log if it is empty
	if sys.stdout.tell() == 0: sys.stdout.close(); os.remove( self.pythonLogFile )
# remove the operator log if it's there and if it doesn't contain the words "warning", "error" or "exception"
#if self.systemLogFile and os.path.isfile( self.systemLogFile ):
#   content = open( self.systemLogFile, 'rb' ).read().decode( 'utf-8' ).lower()
#   if 'warning' not in content and 'error' not in content and 'exception' not in content: os.remove( self.systemLogFile )
