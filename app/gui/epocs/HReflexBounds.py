import sys
import numpy

def ExponentialArtifact( t, height=1.0, logScale=-5.0, t0=0.0, y=None ):
	tScale = numpy.exp( logScale )
	z = ( t0 - t ) / tScale
	artifactUnscaled = numpy.exp( z )
	artifactUnscaled *= ( t >= t0 )
	artifactScaled = height * artifactUnscaled
	if y is None: return artifactScaled
	residuals = y - artifactScaled
	return [
		( -2.0 * residuals * artifactUnscaled        ).sum(), # partial derivative w.r.t. height   of (residuals**2).sum()
		( -2.0 * residuals * artifactScaled * -z     ).sum(), # partial derivative w.r.t. logScale of (residuals**2).sum()
		( -2.0 * residuals * artifactScaled / tScale ).sum(), # partial derivative w.r.t. t0       of (residuals**2).sum()
	]

def SumSquared( a ):
	return ( numpy.asarray( a ) ** 2.0 ).sum()

DEFAULT_RESOLUTION = 9
def RemoveArtifact( contaminated, timebase=None, t0Max=None, nSteps=60, resolution=DEFAULT_RESOLUTION, shrinkFactor=0.5, verbose=False, plot=None ):
	
	if timebase is None: timebase = numpy.arange( contaminated.size ); sampleDuration = 1.0
	else: sampleDuration = numpy.diff( timebase ).mean()
	timebase     = numpy.asarray( timebase     ).ravel()[ :, None, None, None ]
	contaminated = numpy.asarray( contaminated ).ravel()[ :, None, None, None ]
	def ArtifactSumSquaredError( **params ):
		a = ExponentialArtifact( timebase, **params )
		a -= contaminated
		a **= 2.0
		return a.sum( axis=0 ) 
		
	paramNames = 'height logScale t0'.split()	
	defaultResolution = DEFAULT_RESOLUTION
	if isinstance( resolution, int ): defaultResolution, resolution = resolution, {}
	defaultResolution = resolution.pop( '_', defaultResolution )
	for k in paramNames: resolution[ k ] = max( 1, resolution.get( k, defaultResolution ) )
	
	t0Min = max( 0.0, timebase.min() )
	if t0Max is None:
		if   sampleDuration == 1.0:   t0Max = t0Min + 10 
		elif sampleDuration <= 0.001: t0Max = t0Min + 0.003 # assume timebase is in seconds
		else:                         t0Max = t0Min + 3.0   # assume timebase is in milliseconds
		#t0Max = t0Min + ( timebase.max() - t0Min ) * ( 1.0 - 1.0 / resolution[ 't0' ] )   # explore most of the length of the signal (will likely find false spikes)
		
	ranges = dict(
		height = [ min( 0.0, contaminated.min() ) * 2.0, contaminated.max() * 2.0 ],
		logScale = numpy.log( timebase.ptp() / 50.0 ) + numpy.array( [ -2.5, +3.5 ] ),
		t0 = [ t0Min, t0Max ],
	)
	fitted = GridSearch( ArtifactSumSquaredError, ranges, resolution=resolution, nSteps=nSteps, shrinkFactor=shrinkFactor )
	timebase = timebase.ravel()
	contaminated = contaminated.ravel()
	halfSample = numpy.diff( timebase ).mean() / 2.0
	#fitted[ 't0' ] = round( fitted[ 't0' ] / halfSample ) * halfSample
	if verbose: print( '\n'.join( '%20s: [%+12.6f, %+12.6f]  ->  %+g' % ( k, min( ranges[ k ] ), max( ranges[ k ] ), fitted[ k ] ) for k in paramNames ) )
	cleaned = contaminated - ExponentialArtifact( timebase, **fitted )
	cleaned[ numpy.argmin( numpy.abs( timebase - fitted[ 't0' ] ) ) ] = 0.0	

	if isinstance( plot, ( bool, int, type( None ) ) ): doPlot, original = plot, None
	else: doPlot, original = True, plot
	if doPlot:
		Plot( timebase, contaminated, original, 'o-', cleaned, 'x-' )
		plt.legend( [ 'contaminated', 'original', 'cleaned' ] )
	return cleaned
	
def GridSearch( func, ranges, resolution=DEFAULT_RESOLUTION, nSteps=60, shrinkFactor=0.5 ):
	paramNames = list( ranges.keys() )
	defaultResolution = DEFAULT_RESOLUTION
	if isinstance( resolution, int ): defaultResolution, resolution = resolution, {}
	defaultResolution = resolution.pop( '_', defaultResolution )
	for k in paramNames: resolution[ k ] = max( 1, resolution.get( k, defaultResolution ) )
	midpoint = { k : numpy.mean( v ) for k, v in ranges.items() }
	width    = { k : numpy.ptp(  v ) for k, v in ranges.items() }
	grid = { k : numpy.swapaxes( numpy.linspace( -0.5, +0.5, resolution[ k ], endpoint=True )[ :, None, None, None ], 0, axis ) for axis, k in enumerate( paramNames, 1 ) }
	for iStep in range( nSteps ):
		params = { k : midpoint[ k ] + width[ k ] * v for k, v in grid.items() }
		a = func( **params )
		bestIndex = dict( zip( paramNames, numpy.unravel_index( numpy.argmin( a ), a.shape ) ) )
		for k, iBest in bestIndex.items():
			midpoint[ k ] += width[ k ] * grid[ k ].flat[ iBest ]
			width[ k ] *= shrinkFactor
			midpoint[ k ] = max( midpoint[ k ], min( ranges[ k ] ) + 0.5 * width[ k ] )
			midpoint[ k ] = min( midpoint[ k ], max( ranges[ k ] ) - 0.5 * width[ k ] )
	return midpoint

def TestFit( **kwargs ):
	print( kwargs )
	t = Timebase()
	original = numpy.random.normal( size=t.shape )
	contaminated = original + ExponentialArtifact( t, **kwargs )
	cleaned = RemoveArtifact( contaminated, t, verbose=True,  plot=original )
		
		
def Timebase( timeRangeInSeconds=( -0.020, +0.060 ), samplesPerSecond=3200 ):
	return numpy.arange( int( round( min( timeRangeInSeconds ) * samplesPerSecond ) ),
	                     int( round( max( timeRangeInSeconds ) * samplesPerSecond ) ) ) / float( samplesPerSecond )

def Impulse( timeInSeconds, latencyInSeconds=0.025, plot=False ):
	y = numpy.zeros_like( timeInSeconds )
	y[ numpy.argmin( numpy.abs( timeInSeconds - latencyInSeconds ) ) ] = 1.0
	if plot: Plot( timeInSeconds, y )
	return y

def MorletKernel( samplesPerSecond=3200, waveletFrequencyHz=100, waveletEnvelopeParameter=40000, nSigmas=6, plot=False ):
	# waveletEnvelopeParameter = 0.5 / sigma ** 2
	sigma = ( 2 * waveletEnvelopeParameter ) ** -0.5   # = 0.003536  i.e. about 3.5 msec, when waveletEnvelopeParameter is 40000
	halfRangeInSamples = int( round( nSigmas * sigma * samplesPerSecond ) )
	timeInSeconds = numpy.arange( -halfRangeInSamples, halfRangeInSamples + 1 ) / float( samplesPerSecond )
	y = numpy.exp( 2.0j * numpy.pi * waveletFrequencyHz * timeInSeconds - waveletEnvelopeParameter * timeInSeconds ** 2.0 ) 
	if plot: Plot( timeInSeconds, y )
	return y

def Convolve( x, y, axis=-1, output=numpy.abs ):
	if output is complex: output = lambda a: a
	func = lambda x, y: output( numpy.convolve( x, y, 'same' ) )
	return SignalBySignal( axis, func, x, y )
	
def SignalBySignal( axis, func, *inputArrays ):
	"""
	`func` is a function that operates on one-dimensional signals (possibly of
	differing lengths) and outputs a one-dimensional signal::
	
		outputSignal = func( *inputSignals )
		
	The signals are stacked in their respective `inputArrays`, each individual
	signal being a one-dimensional slice of its parent array along the specified
	`axis`.
	
	This function computes an array of output signals by applying `func` to
	each set of corresponding signals from the input arrays (broadcasting as
	necessary).
	"""
	
	if len( set( inputArray.ndim for inputArray in inputArrays ) ) > 1:
		raise ValueError( 'all input arrays must have the same number of dimensions' )
	output = outputMoved = None
	movedArrays = [ numpy.swapaxes( inputArray, axis, 0 ) for inputArray in inputArrays ]  # we use swapaxes because moveaxis is only available in numpy 1.11 and later
	if 0:
		nonSignalShape = numpy.broadcast_shapes( *[ movedArray.shape[ 1: ] for movedArray in movedArrays ] ) # broadcast_shapes is only available in numpy 1.20 and later
	else:
		# so here is a long-hand version:
		nonSignalShape = movedArrays[ 0 ].shape[ 1: ]
		for movedArray in movedArrays[ 1: ]:
			for i, extent in enumerate( movedArray.shape[ 1: ] ):
				if nonSignalShape[ i ] == 1: nonSignalShape[ i ] = extent
				elif extent != 1 and nonSignalShape[ i ] != extent: raise ValueError( 'dimension mismatch' )
		##
	for nonSignalSubscripts in numpy.ndindex( nonSignalShape ):
		inputSignals = [ movedArray[
			(
				slice( None ),
			) + tuple(
				( 0 if extent == 1 else subscript ) for subscript, extent in zip( nonSignalSubscripts, movedArray.shape[ 1: ] )
			)
		] for movedArray in movedArrays ]
		outputSignal = func( *inputSignals )
		if output is None:	
			outputMoved = numpy.empty( ( outputSignal.size, ) + nonSignalShape, dtype=outputSignal.dtype )
			output = numpy.swapaxes( outputMoved, 0, axis )
		outputMoved[ ( slice( None ), ) + nonSignalSubscripts ] = outputSignal
	return output

def PoolByIntensity( intensities, waveforms, timeAxis=-1 ):
	intensities = numpy.asarray( intensities ).ravel()
	waveforms = numpy.swapaxes( waveforms, timeAxis, 0 )[ :, : ]
	if intensities.size != waveforms.shape[ 1 ]: raise ValueError( 'dimension mismatch' )
	uniqueIntensities = numpy.unique( intensities ).tolist()
	nRepetitions = []
	pooledWaveforms = []
	for intensity in uniqueIntensities:
		mask = ( intensities == intensity )
		nRepetitions.append( mask.sum() )
		pooledWaveforms.append( waveforms[ :, mask ].mean( axis=1 ) )
	#pooledWaveforms = numpy.swapaxes( numpy.array( pooledWaveforms ), 1, timeAxis )
	return uniqueIntensities, pooledWaveforms, nRepetitions
		
def FindPeaks( waveform, minProportionalHeight=0.0 ):
	waveform = numpy.asarray( waveform, dtype=float )
	globalMin, globalMax = waveform.min(), waveform.max()
	globalBaseline = 0.0 if globalMax > 0.0 else globalMin
	minPeakHeight = globalBaseline + ( globalMax - globalBaseline ) * minProportionalHeight
	d = numpy.diff( waveform )
	isPeak = numpy.diff( numpy.sign( d ) ) < 0.0
	isPeak[ d[ :-1 ] == 0.0 ] = False
	isPeak[ waveform[ 1:-1 ] < minPeakHeight ] = False
	return isPeak.nonzero()[ 0 ] + 1

def FindMin( waveform, minIndex=0, maxIndex=None ):
	return minIndex + numpy.argmin( waveform[ minIndex : maxIndex ] )
	
def FindMax( waveform, minIndex=0, maxIndex=None ):
	return minIndex + numpy.argmax( waveform[ minIndex : maxIndex ] )
	
def Plot( t, *pargs, **kwargs ):
	axes = kwargs.pop( 'axes', None )
	grid = kwargs.pop( 'grid', True )
	hold = kwargs.pop( 'hold', False )
	import matplotlib.pyplot as plt; ( 'IPython' in sys.modules ) and plt.ion()
	if axes is None: axes = plt.gca()
	if not hold: axes.cla()
	h = []
	for y in pargs:
		if y is None: y = t * numpy.nan
		if isinstance( y, dict ):
			if y: [ eachHandle.set( **y ) for eachHandle in h ]
			continue
		if isinstance( y, str ):
			colorAndMarker = y.rstrip( '-:.' )
			linestyle = y[ len( colorAndMarker ): ]
			if colorAndMarker[ :1 ] in 'rgbcmykw': color, marker = colorAndMarker = colorAndMarker[ :1 ], colorAndMarker[ 1: ]
			else: color, marker = '', colorAndMarker
			for eachHandle in h:
				eachHandle.set_linestyle( linestyle if linestyle else 'none' )
				eachHandle.set_marker( marker if marker else 'none' )
				if color: eachHandle.set_color( color )
			continue
		y = numpy.asarray( y )
		if not y.shape: y = y.ravel()
		y = y.reshape( y.shape[ 0 ], -1 )
		if numpy.iscomplexobj( y ): y = numpy.concatenate( [ y.real, y.imag ], axis=1 )
		h = axes.plot( t, y, **kwargs )
	if grid is not None: axes.grid( grid )
	plt.draw()
	
def FindHReflexBounds(
		rawWaveforms, intensities, samplesPerSecond, timeAxis, timeOfFirstSampleInSeconds=0.0,
		waveletFrequencyHz=100, waveletEnvelopeParameter=40000,
		peakThreshold=0.05, theta=( 0.5, 0.7 ),
		removeArtifact=True,
		plot=False,
	):
	
	rawWaveforms = numpy.swapaxes( rawWaveforms, timeAxis, 0 )
	timeInMsec = 1000.0 * ( timeOfFirstSampleInSeconds + numpy.arange( rawWaveforms.shape[ 0 ] ) / samplesPerSecond )
	
	if removeArtifact:
		rawWaveformsWithArtifacts = rawWaveforms
		rawWaveforms = SignalBySignal( 0, lambda y: RemoveArtifact( y, timebase=timeInMsec, t0Max=3.0 ), rawWaveforms )
		# TODO: very slow
	
	# H-reflex boundaries (McKinnon et al 2023, Section 2.3)
	kernel = MorletKernel( samplesPerSecond=samplesPerSecond, waveletFrequencyHz=waveletFrequencyHz, waveletEnvelopeParameter=waveletEnvelopeParameter )
	convolved = Convolve( rawWaveforms, kernel[ :, None ], axis=0 )
	uniqueIntensities, convolvedAndPooled, nRepetitions = PoolByIntensity( intensities, convolved, timeAxis=0 )
	hull = numpy.max( convolvedAndPooled, axis=0 )
	peakIndices = FindPeaks( hull, minProportionalHeight=peakThreshold )
	penultimatePeakIndex, lastPeakIndex = peakIndices[ -2: ] # TODO: catch the degenerate case where there are <2 peaks
	lastValleyIndex = FindMin( hull, penultimatePeakIndex, lastPeakIndex )
	peakHeight = [ numpy.max( waveform[ lastValleyIndex: ] ) for waveform in convolvedAndPooled ]
	convolvedAndPooledBiggestH = convolvedAndPooled[ numpy.argmax( peakHeight ) ]
	hPeakIndex = FindMax( convolvedAndPooledBiggestH, lastValleyIndex, None )
	normalizedConvolvedAndPooledHAtPeakStim = convolvedAndPooledBiggestH / convolvedAndPooledBiggestH[ hPeakIndex ]
	hStartIndex = FindMin( numpy.abs( theta[  0 ] - normalizedConvolvedAndPooledHAtPeakStim ), lastValleyIndex, hPeakIndex )
	hEndIndex   = FindMin( numpy.abs( theta[ -1 ] - normalizedConvolvedAndPooledHAtPeakStim ), hPeakIndex, None )
	hWaveforms = rawWaveforms[ hStartIndex : hEndIndex + 1, : ]
	
	# H2M  (McKinnon et al 2023, Section 2.4 --- should be identical to the RecruitmentCurve.H2M() method from the RecruitmentCurveFitting toolbox)
	# TODO: make this callable separately as an independent function (and if so, make sure to add the "finesse" correction from the RecruitmentCurve.H2M implementation)
	hReordering = numpy.argsort( -numpy.abs( hWaveforms ).mean( axis=0 ) )
	firstOrderedHWaveformToAverage = 2
	numberOfHWaveformsToAverage = 8
	canonicalH = hWaveforms[ :, hReordering[ firstOrderedHWaveformToAverage : firstOrderedHWaveformToAverage + numberOfHWaveformsToAverage ] ].mean( axis=1 )
	numberOfMWaveformsToAverage = 8
	mReordering = numpy.argsort( intensities )
	fullWaveformAtMMax = rawWaveforms[ :, mReordering[ -numberOfMWaveformsToAverage: ] ].mean( axis=1 )
	mStartIndex = FindMax( numpy.correlate( fullWaveformAtMMax, canonicalH ), 0, lastValleyIndex )
	mEndIndex = mStartIndex + hEndIndex - hStartIndex
	mWaveforms = rawWaveforms[ mStartIndex : mEndIndex + 1, : ]

	if plot:
		rescale = 1.2 * numpy.abs( rawWaveforms ).max() / convolvedAndPooledBiggestH[ hPeakIndex ]
		Plot( timeInMsec, numpy.array( convolvedAndPooled ).T * rescale, color='red' )
		Plot( timeInMsec, rawWaveforms, color=[ 0.7, 0.7, 0.7 ], hold=True )
		#Plot( timeInMsec, hull * rescale, color=[ 0, 1, 0 ], ls='--', hold=True )
		#Plot( timeInMsec[ peakIndices ], hull[ peakIndices ] * rescale, ls='none', marker='+', markersize=20, hold=True )	
		Plot( timeInMsec[ lastValleyIndex: ], convolvedAndPooledBiggestH[ lastValleyIndex: ] * rescale, color=[ 0, 1, 0 ], hold=True )
		Plot( timeInMsec[ hPeakIndex ], convolvedAndPooledBiggestH[ hPeakIndex ] * rescale, ls='none', marker='+', markersize=20, hold=True )
		Plot( timeInMsec[ [ hStartIndex, hEndIndex ] ], convolvedAndPooledBiggestH[ [ hStartIndex, hEndIndex ] ] * rescale, hold=True )
		Plot( timeInMsec[ mStartIndex : mEndIndex + 1 ], mWaveforms, color=[ 0.2, 0.2, 0.2 ], hold=True )
		Plot( timeInMsec[ hStartIndex : hEndIndex + 1 ], hWaveforms, color=[ 0.2, 0.2, 0.2 ], hold=True )
	
	return [ mStartIndex, mEndIndex ], [ hStartIndex, hEndIndex ]

if __name__ == '__main__':

	try: from BCI2000Tools.Plotting import *
	except: import matplotlib.pyplot as plt; ( 'IPython' in sys.modules ) and plt.ion(); look = lambda: None
	else: plt = load_plt()
	plt.close( 'all' )

	if 1:
		samplesPerSecond = 3200.0
		timeInSeconds = Timebase( samplesPerSecond=samplesPerSecond )
		raw = Impulse( timeInSeconds, +0.025 )
		convolved = Convolve( raw, MorletKernel( samplesPerSecond=samplesPerSecond ), output=complex )
		peakIndices = FindPeaks( convolved.real, 0.025 )
		plt.figure()
		Plot( timeInSeconds, raw, marker='o' )
		Plot( timeInSeconds, convolved, marker='x', hold=True )
		Plot( timeInSeconds[ peakIndices ], convolved.real[ peakIndices ], marker='+', markersize=20, linestyle='none', hold=True )
	
	if 1:
		example = numpy.loadtxt( 'example-waveforms.csv', delimiter=',' )
		timeAxis=0
		rawWaveforms = example[ 1:, 1: ]
		timeInSeconds = example[ 1:, 0 ] / 1000.0
		samplesPerSecond = 1.0 / numpy.mean( numpy.diff( timeInSeconds ) )
		intensities = example[ 0, 1: ]
		plt.figure()
		mBoundIndices, hBoundIndices = FindHReflexBounds( rawWaveforms=rawWaveforms, intensities=intensities, samplesPerSecond=samplesPerSecond, timeAxis=timeAxis, plot=True )
		mBounds, hBounds = timeInSeconds[ mBoundIndices ], timeInSeconds[ hBoundIndices ]

	if 1:
		plt.figure()
		TestFit( height=numpy.random.uniform( low=-5.0, high=10.0 ), logScale=numpy.random.uniform( low=-8.0, high=-3.5 ) )
