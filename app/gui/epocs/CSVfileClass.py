import csv

class CSVfileClass( object ):

	def __init__( self, fileName, headerLabels, headerValues, dataLabels, data ):
		self.fileHandle = open( fileName + '.csv', 'wt' )
		self.writer = csv.writer( self.fileHandle )
		self.WriteHeaders( headerLabels, headerValues )
		self.WriteData( dataLabels, data )

	def __del__( self ):
		self.fileHandle.close()

	def WriteHeaders( self, labels, values ):
		#for label, value in zip( labels, values ):
		#	self.writer.writerow( [ label + ': ' + str( value ) ] )

		for label, value in zip( labels, values ):
			line = [ label + ": " ]
			if isinstance( value, list ): line += value
			else: line.append( str( value ) )
			self.writer.writerow( line )

	def WriteData( self, labels, data ):
		self.writer.writerow( labels )
		for d in data:
			self.writer.writerow( d )
