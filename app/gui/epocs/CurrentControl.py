import math
from collections import OrderedDict

try:     import Tkinter as tkinter   # Python 2
except ImportError: import tkinter   # Python 3

from PIL import Image, ImageTk

class Bunch( dict ):
	"""
	A class like a dict but in which you can de-reference members like.this as well as like['this'] .
	Used throughout.
	"""
	def __init__( self, d=(), **kwargs ): dict.__init__( self, d ); dict.update( self, kwargs )
	def update( self, d=(), **kwargs ): dict.update( self, d ); dict.update( self, kwargs ); return self
	def __getattr__( self, key ):
		if key in self.__dict__: return self.__dict__[ key ]
		elif key in self: return self[ key ]
		else: raise AttributeError( "'%s' object has no attribute or item '%s'" % ( self.__class__.__name__, key ) )
	def __setattr__( self, key, value ):
		if key in self.__dict__: self.__dict__[ key ] = value
		else: self[ key ] = value
	def _getAttributeNames( self ): return self.keys()

class CurrentControlWindow(tkinter.Toplevel):
	"""
	###AMIR
	An M Wave Analysis Window is created when the "M-Wave Analysis" button is
	pressed on the ct tab of the GUI().
	"""
	def __init__(self, parent,automate=False):

		tkinter.Toplevel.__init__(self)
		self.t = None # self.t is what we're going to call the StimulusWaveformSelector, apparently
		self.wm_title('Digitimer Current Stimulation Control')
		#self.wm_geometry('225x150')
		self.wm_resizable(width=False,height=False)
		self.wm_attributes("-topmost",1)
		self.withdraw()
		self.parent = parent
		self.protocol('WM_DELETE_WINDOW',self.close)
		self.microamps = 0
		self.StimLocations = OrderedDict()#Bunch()
		self.maxMilliamps = self.parent.operator.params._CurrentLimit
		self.AutomateEnabled = automate

		#self.StimLocations['Location1'] = 0.0 #To hold the threshold value for each location
		self.CurrentAmplitudeState = Bunch(st=[], vc=[], rc=[], ct=[], tt=[])
		self.STDataStorage = []
		self.widgets = Bunch()
		self.pendingMicroampSetting = None
		self.after_id = None
		self.DeferredUpdate()

		self.RCStorage = Bunch(data=[], currents=[], stimpool=[], run=[]) #, log=[], optcurrent=[], use=[], grad=[],Min=[],Max=[],Mtarget=[],nTrials=[],Mwin=[],Hwin=[]
		if self.parent.mode not in ['offline']: self.initUI()

	def initUI(self):

		self.fontLARGE = ('Helvetica', 32)
		fontMED = ('Helvetica', 15)
		fontSMALL = ('Helvetica', 12)
		fontTINY = ('Helvetica', 8)

		#Main Header
		self.bg='white'
		header = tkinter.Frame(self,bg='white')

		#Frame for CurrentLabel
		self.widgets.frame_currentlabel = w = tkinter.LabelFrame(header,bg='white',padx=5,pady=5) #width=10,height=12
		w.grid(row=0, column=0, columnspan=2, rowspan=2, padx=5, pady=5, sticky='nsew')
		w.grid_columnconfigure(0, weight=1)
		w.grid_rowconfigure(0, weight=1)
		#Current Label
		self.currentlabeltxt = tkinter.StringVar()
		self.currentlabel = tkinter.Label(w, font=self.fontLARGE, textvariable=self.currentlabeltxt,bg='white',fg='red',width=8)
		self.SetLabel( self.GetCurrent() )
		self.currentlabel.pack(expand=1,fill='both') #grid(row=0, column=0, columnspan=2,rowspan=2, padx=10, pady=5, sticky='w')

		#Frame for Buttons
		self.widgets.frame_buttons = w = tkinter.LabelFrame(header, bg='white', padx=5,pady=5)  # width=10,height=12
		w.grid(row=0, column=2, columnspan=1, rowspan=2, padx=5, pady=5, sticky='nsew')
		#w.bind('<Configure>', self.resize)
		w.grid_columnconfigure(0, weight=1)
		w.grid_rowconfigure(0, weight=1)

		#Buttons - UP/DOWN
		self.upButton = tkinter.Button(w, text='UP', command= lambda: self.Increment(val=1), font=fontMED,width=6)
		self.downButton = tkinter.Button(w, text='DOWN', command= lambda: self.Increment(val=-1), font=fontMED,width=6)
		self.upButton.pack(side='top',expand=1,pady=3)#grid(row=0, column=2, columnspan=1,rowspan=1, padx=10, pady=5, sticky='nsew')
		self.downButton.pack(side='top',expand=1,pady=3)#grid(row=1, column=2, columnspan=1,rowspan=1, padx=10, pady=5, sticky='nsew')

		#Frame for Increment Label/Text
		self.widgets.frame_increment = w = tkinter.LabelFrame(header, bg='white', padx=5, pady=5)  # width=10,height=12
		w.grid(row=2, column=0, columnspan=3, rowspan=1, padx=5, pady=5, sticky='nsew')
		#w.bind('<Configure>', self.resize)
		w.grid_columnconfigure(0, weight=1)
		w.grid_rowconfigure(0, weight=1)
		#Increment Label/Text
		self.incrementtxt = tkinter.StringVar()
		self.incrementtxt.set(str(self.parent.operator.params._IncrementStart))
		self.incrementlabel = tkinter.Spinbox(w,font=fontMED,from_=-5,increment=self.parent.operator.params._IncrementIncrement,to=5,bg='white',format="%02.3f",width=5,textvariable=self.incrementtxt,command=self.IncrementStep)
		self.incrementlabel.pack(side='left')#grid(row=2, column=0, columnspan=1, padx=3, pady=5, sticky='nsew')
		#self.incrementtxt.trace('w', self.CheckValue)
		self.incrementlabel.config(state='readonly')

		self.label2 = tkinter.Label(w, font=fontMED, text='mA',bg='white',width=3)
		self.label2.pack(side='left')#grid(row=2, column=1, padx=0, pady=5, sticky='nsew')

		self.zeroButton = tkinter.Button( w, text="zero", command=self.Zero, font=fontTINY, width=5 )
		self.zeroButton.pack( side='left' )
		self.unzeroButton = tkinter.Button( w, text="---", state='disabled', font=fontTINY, width=5 )
		self.unzeroButton.pack( side='left' )
		# New button to enable different waveforms. Only create if DS5
		if self.parent.operator.params._DigitimerEnable=='on' and self.parent.operator.params._DigitimerSelect=='DS5':
			self.CreateStimulusWaveformSelector()
			self.StimWaveformButton = tkinter.Button(w, text='>>', command=lambda: self.StimulusWaveformSelector(), font=fontSMALL, width=3).pack(side='right')

		self.Automate = tkinter.IntVar()
		self.AutomateCheck = tkinter.Checkbutton(w, text='Automate', var=self.Automate, bg='white',borderwidth=0.5, selectcolor='white', font=fontMED,command=self.AutomateCheckFunc)
		if self.AutomateEnabled: self.AutomateCheck.pack(side='right')  # grid(row=2, column=2, padx=3, pady=5, sticky='nsew')
		if self.parent.operator.params._DigitimerEnable == 'off': self.AutomateCheck.config(state='disabled')

		header.grid_columnconfigure(0, weight=1)
		header.grid_rowconfigure(0, weight=1)
		header.grid_columnconfigure(1, weight=1)
		header.grid_rowconfigure(1, weight=1)
		header.grid_columnconfigure(2, weight=1)
		header.grid_rowconfigure(2, weight=1)

		header.pack(side='top', fill='both', expand=1)
	
	def Zero( self, value=0.0 ):
		previous = ( self.microamps if self.pendingMicroampSetting is None else self.pendingMicroampSetting ) / 1000.0
		if previous: self.unzeroButton.configure( state='normal', text='%.1fmA' % previous, command=lambda: self.Zero( previous ) )
		self.SetNewCurrent( value )
	
	def IncrementStep(self):

		#If we are automating the recruitment curve and in manual mode... lets update delta in CurrentControl
		if self.AutomateEnabled and self.Automate.get()==1 and self.parent.mode in ['rc'] and self.parent.operator.params._RCendpoint=='Manual':
			if hasattr(self.parent,'ControlObject'):
				#CurrentDelta = self.parent.ControlObject.Delta
				#microamps = self.parent.ControlObject.microamps
				self.parent.ControlObject.Delta = int(float(self.incrementtxt.get())*1000)
				#TODO: Should Delta change also affect the next current (which has already been set?)
				#self.parent.ControlObject.microamps = microamps + ()

	def HighlightButton(self, val):

		#self.variable.set(str(self.values[val]))
		for i in range(6):
			w = self.GroupButtons[i]
			if i == val:
				w['background'] = 'pale green'
				w['relief'] = 'sunken'
			else:
				w['background'] = self.bg
				w['relief'] = 'raised'

		#Change the type of wave used in BCI2000
		self.parent.operator.bci2000('set parameter StimulationType ' + str(int(val+1)))
		self.parent.operator.needSetConfig = True
		#Change setting
		self.parent.operator.params._StimulationType = int(val+1)
		return

	def StimulusWaveformSelector(self):
		if self.t.winfo_viewable() == 1: self.t.withdraw()
		else:
			self.t.deiconify()
			G = self.winfo_geometry().split('+')
			x, y = int(G[1]), int(G[2])
			w, h = [int(i) for i in G[0].split('x')]
			d = '180x225'
			# d = t.winfo_geometry().split('+')[0]
			self.t.geometry(d + '+' + str(x + int(w * 1.05)) + '+' + str(int(y * 0.95)))
		return

	def CreateStimulusWaveformSelector(self):
		#Create a window to select various waveforms (6 at present).
		self.t = tkinter.Toplevel()
		self.t.config(bg=self.bg)
		self.t.wm_title("Waveform Selector")
		self.t.protocol('WM_DELETE_WINDOW', self.t.withdraw)
		font = ('Helvetica', 12)
		fontSmall = ('Helvetica', 10)
		self.GroupButtons = []

		image_directory = "../scraps/Waveforms/"
		waveform_names = ["square","sine","linear_up","linear_down","exp_up","exp_down"]
		#6 waveforms - square,sine,linear_rise,linear_fall,exp_rise,exp_fall
		NumberOfWaveforms = 6

		NoColumnsPerButton = 2

		for i in range(3):
			for j in range(2):
				indx = i*2 +j
				#print(indx)
				w = tkinter.Button(self.t, text="", command=lambda x=(indx): self.HighlightButton(val=x),font=font,bg=self.bg)
				image = Image.open(image_directory+waveform_names[indx]+'.jpg')
				img = ImageTk.PhotoImage(image)
				w.image = img
				#w.config(image=img)
				w.config(width=75,height=50,image=img)
				w.grid(row=i, column=j*NoColumnsPerButton,columnspan=NoColumnsPerButton, sticky='ew', pady=5,padx=5)

				self.GroupButtons.append(w)

		self.HighlightButton(val=int(self.parent.operator.remote.GetParameter('StimulationType')))

		self.label_pw = tkinter.Label(self.t, font=fontSmall, text='ms', bg='white', width=3)
		self.label_pw.grid(row=4,column=1,padx=0,pady=0,sticky='s')  # grid(row=2, column=1, padx=0, pady=5, sticky='nsew')

		#let's add pulsewidth
		self.PWvar = tkinter.StringVar()
		self.PWvar.set(str(self.parent.operator.remote.GetParameter('PulseWidth')).split('ms')[0])
		self.parent.operator.params._PulseWidth = self.PWvar.get()
		self.PWlabel = tkinter.Spinbox(self.t, from_=0.1,increment=0.1, to=3,bg='white', format="%2.1f", width=4, textvariable=self.PWvar,command=self.SetPulseWidth,font=fontSmall)
		self.PWlabel.grid(row=4,column=0,padx=0,pady=0,sticky='s')  # grid(row=2, column=0, columnspan=1, padx=3, pady=5, sticky='nsew')
		# self.incrementtxt.trace('w', self.CheckValue)
		self.PWlabel.config(state='readonly')

		#and biphasic check
		self.Biphasic = tkinter.IntVar()
		self.Biphasic.set(int(self.parent.operator.remote.GetParameter('Biphasic')))
		self.BiphasicCheck = tkinter.Checkbutton(self.t, text='Biphasic?', bg='white',var=self.Biphasic,borderwidth=0.5, selectcolor='white', font=fontSmall,command=self.BiphasicCheckFunc)
		self.BiphasicCheck.grid(row=4,column=3,padx=0,pady=0,sticky='se')
		self.t.withdraw()

	def BiphasicCheckFunc(self):
		#Set BCI2000
		if self.Biphasic.get() == 1: self.parent.operator.bci2000('set parameter Biphasic 1')
		else: self.parent.operator.bci2000('set parameter Biphasic 2')
		#Set Setting
		self.parent.operator.params._Biphasic = self.Biphasic.get()
		return

	def SetPulseWidth(self):
		#Set BCI2000
		self.parent.operator.bci2000('set parameter PulseWidth ' + self.PWvar.get() + 'ms')
		#Set Setting
		self.parent.operator.params._PulseWidth = self.PWvar.get()

	def AutomateCheckFunc(self):
		"""
		For Stimulus test mode, if we are perfoming automated H-reflex threshold detection then we enable the analysis window in this mode
		"""
		return
		#this is for a later release
		#if self.Automate.get() == 1:
		#	EnableWidget(self.parent.MatchWidgets('st','analysis'), True)
		#else:
		#	EnableWidget(self.parent.MatchWidgets('st', 'analysis'), False)

	def resize(self, event):
		return
		#self.font = tkFont.Font(size=int(event.height/3))
		#self.currentlabel.config(font=tkFont.Font(size=int(event.height/3)))

	def CheckValue(self,val,val2,val3):

		#if the increment value is manually changed then make sure it is between 0 and 5mA, and is actually a number
		if (self.incrementtxt.get() != ''):
			try:
				v = float(self.incrementtxt.get())
				if (v<0): self.incrementtxt.set('0.00')
				elif (v>5): self.incrementtxt.set('5.00')
				else: self.incrementtxt.set(v)
			except:
				self.incrementtxt.set('0.5')

	def GetCurrent(self):
		"""
		GetCurrent will return the current stimulus current amplitude in mA

		During a run, it will get the relevant state (from input mode).
		Else, if not running, it will get the InitialCurrent parameter
		If SetConfig has been set, then self.microamps will hold the current value.

		"""

		#Check if the system has started running or at least preflight
		if self.parent.operator.started:
			#Extract current amplitude from State CurrentAmplitude if we Running == 1
			self.microamps = self.parent.operator.remote.GetStateVariable( 'CurrentAmplitude' ).value # self.parent.states[self.parent.mode].CurrentAmplitude
			milliamps = self.microamps / 1000.0
			self.SetLabel(milliamps)
			return milliamps
		elif (self.parent.operator.needSetConfig):
			milliamps = float(self.parent.operator.remote.GetParameter('InitialCurrent'))
			self.microamps = int(0.5 + milliamps*1000)
			self.SetLabel(milliamps)
			return milliamps
		else:
			return ( self.microamps if self.pendingMicroampSetting is None else self.pendingMicroampSetting ) / 1000.0

	def SetLabel(self,value):
		"""
		This sets the Current Control Window Current Label

		Value = Stimulus current in mA or uA
		"""

		LabelTxt = "%.2f" % value#str(value) + 'mA'
		self.currentlabeltxt.set(LabelTxt + 'mA')

	def Increment(self,val):
		"""
		Increment increases/decreases the current stimulii (mA). It is triggered by the up and down buttons of the GUI

		val = 1 if up, -1 if down. Increments are determined by the IncrementValue in the label of the GUI.
		"""

		#When first loaded, we have not done a preflight so adjusting the value will not have an effect yet
		#Read Increment Value
		#if not self.CheckValue(val=self.incrementlabel.get()): return

		if (self.Automate.get() == 1) and not(self.parent.operator.params._RCendpoint == 'Manual'): return  # cannot increment when automated unless manual selected

		incrementMilliamps = abs( float( self.incrementlabel.get() ) )
		incrementMicroamps = math.floor( 1000 * incrementMilliamps )
		#Read Current Value
		oldMicroamps = self.microamps if self.pendingMicroampSetting is None else self.pendingMicroampSetting

		newMicroamps = oldMicroamps + val * incrementMicroamps

		if newMicroamps < 0: newMicroamps = 0
		newMicroamps = max( 0, min( int( 1000 * self.maxMilliamps ), newMicroamps ) )
		
		self.SetLabel( newMicroamps / 1000.0 )  # make the cosmetic change now,
		self.DeferredUpdate(newDesiredMicroamps=newMicroamps) # but allow the option of deferring sending it to BCI2000

		if (self.Automate.get() == 1) and (self.parent.operator.params._RCendpoint == 'Manual'):
			if hasattr(self.parent,'ControlObject'): self.parent.ControlObject.microamps = self.microamps


	def DeferredUpdate( self, newDesiredMicroamps='idle' ):
		if newDesiredMicroamps == 'idle':
			if self.pendingMicroampSetting is None:
				# The user has not clicked to change the current recently.  Query BCI2000 to see if the current has been changed via the physical dial.
				if self.parent.operator.started:
					microamps = self.parent.operator.remote.GetStateVariable( 'CurrentAmplitude' ).value # self.parent.states[self.parent.mode].CurrentAmplitude
					self.SetNewCurrent( microamps / 1000.0, sendToBCI2000=False )
			else:
				# The user has clicked to change the current recently (but at least 1 cycle ago).  Tell BCI2000 to update the stimulator.
				self.SetNewCurrent( self.pendingMicroampSetting / 1000.0, sendToBCI2000=True )
				self.pendingMicroampSetting = None			
		else:
			# The user has clicked to change the current, just now.
			# Restart the after() clock and defer sending this to BCI2000 until one cycle from now, unless another change comes along in the meantime.
			# The reason for this is that some stimulator firmware/hardware versions require a disarm/re-arm workaround for changing current via the API
			# while armed, and when the user click-click-clicks rapidly to change the current, the stimulator relays start clicking audibly and alarmingly
			# with every update. Deferment allows us to rate-limit these updates: the user's click-changes get aggregated, and only sent to the stimulator
			# when the user has not clicked for the duration of one whole cycle.
			try: self.after_cancel( self.after_id ) # cancel any already-pending update
			except: pass
			
			# UPDATE: aggregation of demand changes is no longer necessary when we're using the upgraded DS8 API and no longer have to disarm/rearm.
			# So now DeferredUpdate() will never actually store its microamp argument (unless we set aggregateChanges to True, below).
			# However, let's leave this whole mechanism in place. Either way, we still need the DeferredUpdate() cycle, to update the GUI based on
			# manual dial twiddles, and so it's good to have after_cancel() in place, above, so that we inhibit information flow in that direction for
			# one cycle while a new value gets set.
			aggregateChanges = False
			if aggregateChanges: self.pendingMicroampSetting = newDesiredMicroamps 
			else: self.SetNewCurrent( newDesiredMicroamps / 1000.0 )
			
		self.after_id = self.after( 500, self.DeferredUpdate ) # Whatever else has happened, renew the cycle.

	def SetNewCurrent(self, milliamps=None, sendToBCI2000=True):
		"""
		SetNewCurrent is the master controller for setting the new current when it is changed automatically or manually.

		If run started DesiredMicroamps state is set, and BCI2000 is told that this value is changed (state NeedsUpdating)
		If not running then the parameter InitialCurrent is set.
		
		NB: the old state variable, CurrentAmplitude, that used to be used for this purpose, is still in place, and
		can still be used for analysis. But it should be considered read-only. It is more useful now, because it actually
		reflects whatever the current has been set to, regardless of whether this was by API or by manual dial-turning.
		"""
		if milliamps is None:
			microamps = self.microamps
			milliamps = microamps / 1000.0
		else:
			microamps = int(0.5 + milliamps*1000)
		self.SetLabel(milliamps)
		self.microamps = microamps
		if sendToBCI2000:
			#If not started or it exists (as in running has been 1, then we change current amplitude
			if self.parent.operator.started:
				self.parent.operator.bci2000('Set State DesiredMicroamps %d' % microamps)
				self.parent.operator.bci2000('Set State NeedsUpdating 1')
			else: #Not started before
				self.parent.operator.bci2000('Set Parameter InitialCurrent %g' % milliamps)
				self.parent.operator.needSetConfig = True		

	def addLocation(self,value=0):
		"""
		Used in stimulus test mode, this function adds a new stimulation location that default to Location0, Location1 etc

		val = Location name, value1 = Hthreshold for that location, value2 = H-reflex Amplitude at this threshold
		"""
		n = len(self.StimLocations)
		val = 'Location'+str(n)
		self.StimLocations[val] = value

	def close(self):
		"""
		Withdraw the stimulus control window, do not delete it

		Deleting it will mean that control of the current will be lost without saving the variables globally, easier to withdraw
		"""
		if self.parent.operator.started: return
		else:
			self.withdraw()
			if self.t is not None: self.t.withdraw()

