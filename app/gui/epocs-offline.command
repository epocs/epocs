#!/bin/bash

HERE=$( dirname "${BASH_SOURCE[0]}" )

ANACONDA_WRAPPER=
#ANACONDA_WRAPPER=anaconda3
#which $ANACONDA_WRAPPER >/dev/null 2>/dev/null || ANACONDA_WRAPPER=

PYTHONPATH="$HERE:$HERE/../tools/python" $ANACONDA_WRAPPER \
python -u -m epocs --offline "$@"    2>&1                  \
| grep --line-buffered -v DS5LibClass                      \
| grep --line-buffered -v FIFinderSyncExtensionHost        \
| grep --line-buffered -v IMKClient                        \
;
