@echo off
set "PWS=powershell.exe -ExecutionPolicy Bypass -NoLogo -NonInteractive -NoProfile

for /f "usebackq tokens=*" %%a in (`%PWS% -Command "$([Environment]::GetFolderPath('Desktop'))"`) do set "DESKTOP=%%a

set "DESTINATION=%DESKTOP%
%PWS% -Command "$ws = New-Object -ComObject WScript.Shell; $s = $ws.CreateShortcut('%DESTINATION%\EPOCS.lnk');                  $s.TargetPath = '%~dp0epocs.bat';         $s.WorkingDirectory = '%~dp0'; $s.IconLocation = '%~dp0epocs.ico'; $s.Save()"
%PWS% -Command "$ws = New-Object -ComObject WScript.Shell; $s = $ws.CreateShortcut('%DESTINATION%\EPOCS Offline Analysis.lnk'); $s.TargetPath = '%~dp0epocs-offline.bat'; $s.WorkingDirectory = '%~dp0'; $s.IconLocation = '%~dp0epocs.ico'; $s.Save()"
::%PWS% -Command "$ws = New-Object -ComObject WScript.Shell; $s = $ws.CreateShortcut('%DESTINATION%\EPOCS Offline Analysis.lnk'); $s.TargetPath = '%~dp0epocs.bat';         $s.WorkingDirectory = '%~dp0'; $s.IconLocation = '%~dp0epocs.ico'; $s.Arguments = '--offline'; $s.Save()"
