@echo off

:: lastlog operator
:: lastlog python

set "LOGDIR=%~dp0..\..\system-logs
set "NEWEST=%LOGDIR%
for /f "tokens=*" %%a in ('dir /b /od %LOGDIR%\*%1.txt') do set "NEWEST=%LOGDIR%\%%a
start %NEWEST%