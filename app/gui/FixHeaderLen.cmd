#!/usr/bin/env python
""" " 2>NUL
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
setlocal
set "CUSTOMPYTHONHOME=%PYTHONHOME_EPOCS%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*
endlocal
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

__doc__ = """
In a BCI2000 .dat file, before the binary data starts, there is a plain text header
containing, among other things, the parameters for the run. The very first line of
the header specifies how long the header is (i.e. the offset of the binary data) as
a decimal integer string following the field name `HeaderLen= `.

Occasionally, it may be necessary to edit the parameters that appear in the header.
If this results in a change in the header length, the file will be corrupted unless
the `HeaderLen` value is adjusted accordingly. This utility performs the necessary
adjustment.

This utility takes two filenames as arguments.  The first should be your original
.dat file, before you edited the parameters---this file will not be altered. The
second filename should be your edited .dat file, incorporating your changes to the
parameters. The edited file will be overwritten, with a change to its first line
if necessary to reflect the updated HeaderLen.
"""

import os, sys, ast, collections
class UsageError( Exception ): pass

if sys.version < '3': bytes = str
else: unicode = str; basestring = ( unicode, bytes )
def IfStringThenRawString( x ):
	return x.encode( 'utf-8' ) if isinstance( x, unicode ) else x
def IfStringThenNormalString( x ):
	if str is bytes or not isinstance( x, bytes ): return x
	try: return x.decode( 'utf-8' )
	except: pass
	try: return x.decode( sys.getfilesystemencoding() )
	except: pass
	return x.decode( 'latin1' ) # bytes \x00 to \xff map to characters \x00 to \xff (so, in theory, cannot fail)

def ReadFile( filename ):
	filename = os.path.realpath( filename )
	content = open( filename, 'rb' ).read()
	firstLineLength = content.index( IfStringThenRawString( '\n' ) ) + 1
	firstLine, remainder = content[ :firstLineLength ], content[ firstLineLength: ]
	info = IfStringThenNormalString( firstLine ).strip().split()
	info = collections.OrderedDict( ( k.rstrip( '=' ), ast.literal_eval( v ) ) for k, v, in zip( info[ ::2 ], info[ 1::2 ] ) )
	return filename, info, firstLine, remainder

def MakeHeaderLine( d ):
	return IfStringThenRawString( ' '.join( '%s= %s' % ( k, v ) for k, v, in d.items() ) + '\r\n' )

def Main( beforeFileName, afterFileName ):
	beforeFileName, infoBefore, firstLineBefore, remainderBefore = ReadFile( beforeFileName )
	afterFileName,  infoAfter,  firstLineAfter,  remainderAfter  = ReadFile( afterFileName  )
	#if( firstLineAfter != firstLineBefore ): raise UsageError( 'initial lines mismatched: are you sure these are versions of the same file?' )
	previousFirstLineAfter = firstLineAfter
	change = changeInRemainder = len( remainderAfter ) - len( remainderBefore ) 
	changeInFirstLine = 0
	for i in range( 3 ):
		infoAfter[ 'HeaderLen' ] = infoBefore[ 'HeaderLen' ] + change
		firstLineAfter = MakeHeaderLine( infoAfter )
		changeInFirstLine = len( firstLineAfter ) - len( firstLineBefore )
		if change == changeInFirstLine + changeInRemainder: break
		change = changeInFirstLine + changeInRemainder
	else:
		raise UsageError( 'failed to converge' )
		
	contentBefore = ( firstLineBefore + remainderBefore ); del remainderBefore
	contentAfter  = ( firstLineAfter  + remainderAfter  ); del remainderAfter 
	print( 'before edit:  %r' % firstLineBefore )
	print( ' after edit:  %r' % previousFirstLineAfter  )
	print( '  corrected:  %r' % firstLineAfter  )
	if previousFirstLineAfter == firstLineAfter:
		print( "no change necessary" )
	else:
		bodyBefore = contentBefore[ infoBefore[ 'HeaderLen' ]: ]
		bodyAfter  = contentAfter[  infoAfter[  'HeaderLen' ]: ]
		if bodyBefore != bodyAfter:
			print( repr( bodyBefore[ :100 ] ) )
			print( repr( bodyAfter[  :100 ] ) )
			raise UsageError( 'data mismatch' )
		print( 'writing update to %s' % afterFileName )
		open( afterFileName, 'wb' ).write( contentAfter )
	
	
if __name__ == '__main__':
	
	execPath, args = sys.argv[ 0 ], sys.argv[ 1: ]
	execName = os.path.basename( execPath )
	execPath = os.path.realpath( execPath )
	if sys.platform.lower().startswith( 'win' ): execName = os.path.splitext( execName )[ 0 ]
	
	try:
		if len( args ) != 2: raise UsageError( "\n    usage:   %s ORIGINAL EDITED" % execName )
		Main( sys.argv[ 1 ], sys.argv[ 2 ] )
	except ( UsageError, IOError ) as error:
		sys.stderr.write( '%s failed - %s\n' %( execName, error ) )
