#ifndef   __VariableDelayMenu_H__
#define   __VariableDelayMenu_H__

////////////////////////////////////////////////////////////////////////////////////////////////

#define MODIFIED_LEFT_ALWAYS_GOES_BACK 1


const char MAIN_DOC[] =
	"Turn the selector wheel left or right to navigate menu options, and click the selector to select a menu item. "
	"Menu items in ALL-\tCAPS generally perform an action or re-\tconfiguration immediately when clicked. "
#	if MODIFIED_LEFT_ALWAYS_GOES_BACK
	"You can also press the selector and hold it in while turning: press-\tand-\tturn-\tleft is a shortcut for selecting \"GO BACK\" to navigate back up the menu tree; press-\tand-\tturn-\tright is the same as clicking, "
	"with one exception: if you are adjusting a numeric value like a pulse delay, and the adjustment delta value is shown on the right, then press-\tand-\tturn-\tright allows you to change the delta. "
#	else
	"If you are adjusting a numeric value such as a pulse delay, you can also press the selector button and hold it in while turning: this lets you increase or decrease the adjustment delta. "
	"Anywhere else in the menu, holding the selector pressed-\tin while turning left is a shortcut for selecting \"GO BACK\" to navigate back up the menu tree. "
#	endif
	"\n!! SAFETY REMINDER: you must ensure that appropriate isolation is implemented between the SyncGenie and any stimulator/\tsensor circuitry that contacts a human research subject. Most stimulators, amps and headsets do incorporate it, but you must verify this, and build your own isolation solution if not. "
	"\nTo exit this doc screen, you can use the press-\tand-\tturn-\tleft gesture, or just click the selector once. "
;


void AssignPin( int & param, int pinNumber )
{
	if( gOutputPin == pinNumber ) gOutputPin = -1;
	if( gInputPin  == pinNumber ) gInputPin  = -1;
	param = pinNumber;
	configure();
}

void RenderNumericParameter( const char * valueFmt, double value, int millideltas=0 )
{
	static char * buffer = NULL;
	if( !buffer ) buffer = new char[ genie.LCD_COLS + 1 ];
	size_t valueLength = snprintf( buffer, genie.LCD_COLS + 1, valueFmt, value );
	size_t spaceLeft = ( valueLength < ( size_t )genie.LCD_COLS ) ? ( size_t )genie.LCD_COLS - valueLength : 0;
	String deltaString;
	switch( millideltas )
	{
		case       0: deltaString =          ""; break;
		case       1: deltaString =    "+0.001"; if( spaceLeft < deltaString.length() ) deltaString = "+1e-3"; break;
		case      10: deltaString =    "+0.010"; if( spaceLeft < deltaString.length() ) deltaString = "+0.01"; break;
		case     100: deltaString =    "+0.100"; if( spaceLeft < deltaString.length() ) deltaString = "+0.1";  break;
		case    1000: deltaString =    "+1";     break;
		case   10000: deltaString =   "+10";     break;
		case  100000: deltaString =  "+100";     break;
		case 1000000: deltaString = "+1000";     if( spaceLeft < deltaString.length() ) deltaString = "+1e3";  break;
	}
	if( spaceLeft < deltaString.length() && deltaString[ 1 ] == '0' ) deltaString = "+" + deltaString.substring( 2 );
	if( spaceLeft < deltaString.length() ) deltaString = deltaString.substring( 1 );
	if( deltaString[ 0 ] == '+' ) deltaString[ 0 ] = '\x01';
	for( ; spaceLeft > deltaString.length(); spaceLeft-- ) sprintf( buffer + valueLength++, " " );
	if( spaceLeft >= deltaString.length() ) sprintf( buffer + valueLength, "%s", deltaString.c_str() );
	genie.lcd.print( buffer );
}

typedef enum { NOT_AN_ADJUSTMENT, DELTA_ADJUSTMENT, TARGET_ADJUSTMENT } AdjustmentResult;
AdjustmentResult AdjustUint32( int direction, uint32_t & param, bool modifiedNavigation, uint32_t & delta, uint32_t minDelta, uint32_t maxDelta )
{
	if( modifiedNavigation )
	{
#		if MODIFIED_LEFT_ALWAYS_GOES_BACK
			if( direction < 0 ) return NOT_AN_ADJUSTMENT; // do not override standard navigation logic
			if( delta >= maxDelta ) delta = minDelta; // wrap around
			else delta *= 10;
#		else
			if( direction > 0 && delta <  maxDelta      ) delta *= 10;
			if( direction < 0 && delta >= minDelta * 10 ) delta /= 10;
#		endif
		return DELTA_ADJUSTMENT;
	}
	else
	{
		if( direction > 0 ) param += delta;
		if( direction < 0 ) param = ( param > delta ) ? param - delta : 0;
		reportChanges();  // NB: this command is specific to the VariableDelay sketch
		return TARGET_ADJUSTMENT;
	}
}

uint32_t gDeltaMicros    =  10000; // a shared delta setting for all the microsecond-scale parameters (pulseDurationMicros, pulsePeriodMicros, burstPeriodMicros)
void RenderMicros( uint32_t param ) { RenderNumericParameter( "%5.3f", param / 1000.0, gDeltaMicros ); } // render (in milliseconds) one of the parameters that is internally represented in microseconds
bool AdjustMicros( int direction, uint32_t & param, bool modifiedNavigation=false )
{
	switch( AdjustUint32( direction, param, modifiedNavigation, gDeltaMicros, 100, 100000 ) )
	{
		case DELTA_ADJUSTMENT:               return true;  // the left/right gesture has caused a change in some numeric variable rather than a standard navigation step, so return true to override standard navigation logic
		case TARGET_ADJUSTMENT: configure(); return true;  // the left/right gesture has caused a change in some numeric variable rather than a standard navigation step, so return true to override standard navigation logic
		default:                             return false; // do not override standard navigation logic
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

#include "VariableDelayMenuAuto.h" // header auto-generated by `MakeMenu.py`, defines the `Menu` class (will reference global variables from main sketch and global helper-function definitions above)
Menu gMenu( genie.lcd ); // instantiate the menu system using the global `genie`

void menuBegin( void )
{
	gMenu.begin();  // this enables the menu system, starting us off at the root menu node
	
	byte delta[ 8 ] = {
		0b00000,
		0b00000,
		0b00100,
		0b01010,
		0b01010,
		0b10001,
		0b11111,
		0b00000,
	};
	genie.lcd.createChar( 1, delta );
}

void menuRefresh( void )
{
	gMenu.render();
}

void menuNavigation( void )
{
	int selectorWheelEvent  = genie.selectorWheel.event();  // will be zero unless the selector has been turned left or right since the previous call
	int selectorButtonEvent = genie.selectorButton.event(); // will be zero unless the selector has been pressed or released since the previous call

	// Handle selector press and release events:
	if( selectorButtonEvent == Switch::PRESS   ) gMenu.modifyNavigation = true;
	if( selectorButtonEvent == Switch::RELEASE ) { gMenu.modifyNavigation = false; gMenu.select(); } // selection actually happens when you release the selector
	
	// If you're pressing-and-turning, let's ensure your subsequent release of the selector is not interpreted as an additional "click":
	if( selectorWheelEvent && gMenu.modifyNavigation ) gMenu.ignoreNextSelection();
	
	// Handle wheel-turn events:
	if( selectorWheelEvent == Wheel::LEFT     ) gMenu.left();
	if( selectorWheelEvent == Wheel::RIGHT    ) gMenu.right();
	
	if( gInputPin != genie.BUTTON && genie.pushButton.event() == Switch::RELEASE ) gMenu.gotoNode( "/Pulse Delay (ms)/x" ); // NB: this logic and destination are specific to the VariableDelay sketch
}

////////////////////////////////////////////////////////////////////////////////////////////////
#endif // __VariableDelayMenu_H__
