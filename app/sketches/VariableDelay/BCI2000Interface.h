bool publishing = keyhole.command( "publish" );

#define PARAM( TAB, SECTION, TYPE, BCI2000NAME, KEYHOLENAME, VARIABLE, MIN, MAX, COMMENT ) \
	if( publishing ) keyhole << ( TAB ) << ":" << ( SECTION ) << " " << ( #TYPE ) << " " << ( BCI2000NAME ) << "= " << ( VARIABLE ) << " " << ( VARIABLE ) << " " << ( MIN ) << " " << ( MAX ) << " // " << ( COMMENT ); \
	else keyhole.alias( BCI2000NAME, KEYHOLENAME );

PARAM( "Triggering", "Trigger%20Delay", int, "WidgetInputPin",                     "InputPin",             gInputPin,             "%", "%", "microcontroller pin number on which to read the incoming trigger pulse" );
PARAM( "Triggering", "Trigger%20Delay", int, "WidgetTriggerPolarity",              "TriggerPolarity",      gTriggerPolarity,        1,   2, "incoming trigger mode: 1 Rising Edge, 2 Falling Edge (enumeration)" );
PARAM( "Triggering", "Trigger%20Delay", int, "InitialTriggerDelayInMicroseconds",  "DelayMicros",          gDelayMicros,            0, "%", "trigger delay setting at the start of each run" );
PARAM( "Triggering", "Trigger%20Delay", int, "WidgetOutputPin",                    "OutputPin",            gOutputPin,            "%", "%", "microcontroller pin number on which to output a delayed rising-edge trigger pulse" );
PARAM( "Triggering", "Trigger%20Delay", int, "WidgetOutputDurationInMicroseconds", "OutputDurationMicros", gOutputDurationMicros, "%", "%", "duration of output trigger pulse in microseconds" );

if( publishing )
{
	keyhole << "TriggerDelayInMicroseconds 32 0 0 0";


	String info = INTROSPECTION;	
	// remove the last 2 lines (horizontal rule and "Click to exit."):
	size_t newLength = info.length();
	for( size_t linesRemoved = 0; newLength > 0 && linesRemoved < 2; newLength-- )
		if( info[ newLength - 1 ] == '\n' ) linesRemoved++;
	if( newLength < info.length() ) info = info.substring( 0, newLength );
	// put it all on one line:
	info.replace( "\n", "; " );
	// BCI2000-parameter-escape sensitive characters:
	char match[ 2 ] = "%", replace[ 4 ] = "%25";
#	define BCI2000_ESCAPE( C ) match[ 0 ] = C; snprintf( replace, 4, "%%%02x", ( int )( ( unsigned char )C ) ); info.replace( match, replace );
	BCI2000_ESCAPE( '%' ); // must be first
	BCI2000_ESCAPE( '{' ); BCI2000_ESCAPE( '}' );
	BCI2000_ESCAPE( '[' ); BCI2000_ESCAPE( ']' );
	BCI2000_ESCAPE( '?' ); BCI2000_ESCAPE( ' ' );
	keyhole << "Triggering" << ":" << "Trigger%20Delay" << " string WidgetInfo= " << info << " % % % // SyncGenie programming and versioning info (readonly)";
		
	keyhole << ""; // blank line to signify end of definitions
}

if( keyhole.command( "update" ) )
{
	keyhole << "InitialTriggerDelayInMicroseconds = " << gDelayMicros;
	keyhole << "";
}