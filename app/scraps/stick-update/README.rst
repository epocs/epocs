This directory contains two scripts that can be put on a memory stick, next to a bare repository.
First run ``git clone --bare https://bitbucket.org/epocs/epocs`` on the stick, so that it contains
``epocs.git``.  Then copy these two scripts next to ``epocs.git``.

``update_stick_from_bitbucket.bat`` can be used on any OS, from a network-enabled computer, to
update the bare repository on the stick with any changesets that are present on bitbucket.

``update_c-neurotech-epocs_from_stick.bat`` can be used on a non-networked Windows computer
to pull changes from the stick to the canonical location ``C:\neurotech\epocs``. For this to
work, one of the following must be true:

* EITHER: ``C:\neurotech\epocs`` is already newer than commit 9074989 (2021-07-06 12:29:30 -0400),

* OR: ``C:\neurotech\epocs`` already has its default origin set to the path to this bare
  repository, on this stick.
