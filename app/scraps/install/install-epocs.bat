@echo off
setlocal enabledelayedexpansion

set "EPOCS_URL=https://bitbucket.org/epocs/epocs
set "FM275_URL=https://bitbucket.org/jezhill/FullMonty275
set "GITSETUP=Git-2.46.2-64-bit.exe
set "GITOPTS_IN=%USERPROFILE%\Downloads\git-install.ini
set "GITOPTS_OUT=%USERPROFILE%\Downloads\git-installed.ini

set "DEFAULT_TARGET=C:\neurotech\epocs"

echo.
echo ======================================================
echo Checking for git...

git help >NUL 2>NUL && goto :gotgit

echo git does not seem to be installed.
echo I will try to install it for you...
echo.
pause

echo [Setup]                                                         >  %GITOPTS_IN%
echo Lang=default                                                    >> %GITOPTS_IN%
echo NoIcons=0                                                       >> %GITOPTS_IN%
echo SetupType=default                                               >> %GITOPTS_IN%
echo Components=ext,ext\shellhere,ext\guihere,gitlfs,assoc,assoc_sh  >> %GITOPTS_IN%
echo Tasks=                                                          >> %GITOPTS_IN%
echo EditorOption=VIM                                                >> %GITOPTS_IN%
echo CustomEditorPath=                                               >> %GITOPTS_IN%
echo DefaultBranchOption=main                                        >> %GITOPTS_IN%
echo PathOption=Cmd                                                  >> %GITOPTS_IN%
echo SSHOption=OpenSSH                                               >> %GITOPTS_IN%
echo TortoiseOption=false                                            >> %GITOPTS_IN%
echo CURLOption=OpenSSL                                              >> %GITOPTS_IN%
echo CRLFOption=CRLFCommitAsIs                                       >> %GITOPTS_IN%
echo BashTerminalOption=MinTTY                                       >> %GITOPTS_IN%
echo GitPullBehaviorOption=Merge                                     >> %GITOPTS_IN%
echo UseCredentialManager=Core                                       >> %GITOPTS_IN%
echo PerformanceTweaksFSCache=Enabled                                >> %GITOPTS_IN%
echo EnableSymlinks=Enabled                                          >> %GITOPTS_IN%
echo EnablePseudoConsoleSupport=Disabled                             >> %GITOPTS_IN%

if exist %GITSETUP% goto :gotgitsetup
echo git could not be installed because I could not
echo find %GITSETUP% - download it from
echo %EPOCS_URL%/downloads
echo and put it next to this script (or install git
echo yourself from https://git-scm.com ) before
echo re-running this script.
echo.
pause
exit /b 1

:gotgitsetup
%GITSETUP% /LOADINF="%GITOPTS_IN" /SAVEINF="%GITOPTS_OUT" /SILENT 2>NUL && echo git has been installed. && echo Please re-launch this console/script. && exit /b 1

echo %GITSETUP% failed to install git
echo Please install git manually using the official
echo installer downloaded from https://git-scm.com
echo and then re-run this script.
echo.
pause
exit /b 1

:gotgit
echo git is available
echo ======================================================
echo.
set "TARGET=%~df1"
if "%TARGET%"=="" set /p "TARGET=Installation location [%DEFAULT_TARGET%]: "
if "%TARGET%"=="" set "TARGET=%DEFAULT_TARGET%"
for %%I in ("%TARGET%") do (
	set "PARENT=%%~dpI"
	set "NAME=%%~nxI"
)
if exist "%TARGET%" echo "%TARGET%" already exists. && echo Cancelling installation. && echo. && pause && exit /b 1
if not exist "%PARENT%" mkdir "%PARENT%"
if not exist "%PARENT%" echo could not make directory "%PARENT%" && exit /b 1
::echo "%PARENT%" && echo "%NAME%" && pause && exit /b 0
set "OLDDIR=%CD%
cd /D "%PARENT%"
echo ======================================================
echo Installing epocs (this may take a few minutes)...
echo ======================================================
git clone %EPOCS_URL% "%NAME%"
cd "%NAME%\App\gui
echo ======================================================
echo Installing embedded Python interpreter...
echo ======================================================
git clone %FM275_URL% interpreter
echo ======================================================
echo Making desktop shortcuts...
echo ======================================================
call shortcuts.bat
echo Done
echo ======================================================
pause
