The following step-by-step guide, published in the Journal of Visualized
Experiments in 2022, is intended to introduce potential end-users to EPOCS:

- Article (pdf, 22 pages): https://bit.ly/Hill2022-JoVE-EPOCS
- Video (mp4, 10 minutes): https://bit.ly/Hill2022-JoVE-EPOCS-Video
- Citation:
  
      Hill NJ, Gupta D, Eftekhar A, Brangaccio JA, Norton JJ, McLeod M, Fake T,
      Wolpaw JR & Thompson AK (2022). The evoked potential operant conditioning
      system (EPOCS): a research tool and an emerging therapy for chronic
      neuromuscular disorders. Journal of Visualized Experiments
      2022 Aug 25(186):e63736. doi:10.3791/63736; PMID: 36094287; PMCID: 9948679

As explained in detail in the article, EPOCS requires 64-bit Windows, and you will
need to begin by installing NI-DAQmx (including 64-bit support).

If you are using a Digitimer DS8R stimulator (for which EPOCS supports automation),
you should also install its latest driver and control software from the Digitimer
website.

To install EPOCS itself, you will also need git. If you already have git installed
on your system:

	1. Download `install-epocs.bat`
	
	2. Launch `install-epocs.bat`

(Ignore the "Download repository" link in this directory. That has been put here
automatically and we can't get rid of it, but it is not what you want.)

On the other hand, if you do not already have git:

	1. Download `install-epocs.bat`.
	
	2. Download the git installer (`Git-2.46.2-64-bit.exe`, also available here*)
	   into the same folder as `install-epocs.bat` (but you do not need to run the
	   git installer manually).
	  
	3. Launch `install-epocs.bat`. It will install git for you (without forcing
	   you to answer the usual long list of technical questions), and then tell
	   you to...

	3. Launch `install-epocs.bat` again, to actually install EPOCS.

*NB: if you prefer to have the latest/greatest version of `git` itself, and the
git installer we provide is out of date, you can always run the command
`git update-git-for-windows` at some later date: then git will self-update.


Assuming successful installation, launch EPOCS from the desktop icon.
It will assume you have a National Instruments device installed/connected.
On first run, a small window will appear asking you to configure the device.
Usually we choose P0.7 as the digital output port. Whichever one you choose,
make sure it is wired to the terminal or cable that you want to use for
triggering (see the EPOCS article for more-precise instructions).


