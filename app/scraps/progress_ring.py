import matplotlib, matplotlib.pyplot as plt
plt.ion()

class ProgressRing( object ):
    def __init__( self, nMax=1.0, figure=None, axes=None ):
        if axes is None:
            if figure is None: figure = plt.gcf()
            try: figure = figure.number
            except: pass
            axes = plt.figure( figure ).gca() 
        self.axes = axes
        self.axes.cla()
        
        self.axes.set( xlim=[ -1, 1 ], ylim=[ -1, 1 ], aspect=1, frame_on=False, xticks=[], yticks=[] )
        self.wedge = matplotlib.patches.Wedge( center=(0,0), r=1, theta1=0.0, theta2=0.0, width=0.5, ec='none', fc='#AAFFAA' )
        self.text = self.axes.text( 0, 0, '0', ha='center', va='center', fontsize=100, fontweight='bold' )
        self.axes.add_patch( self.wedge )
        self.Update( n=0, nMax=nMax )
        
    def Update( self, n=None, nMax=None ):
        if n    is not None: self.__n    = n
        if nMax is not None: self.__nMax = nMax
        p = self.__n / float( self.__nMax )
        self.wedge.set( theta2=90, theta1=90 - 360 * p )            
        self.text.set( text=str( self.__n ) )
        
        plt.draw()
    
    n    = property( fget=lambda self: self.__n,    fset=lambda self, value: self.Update(    n=value ) )
    nMax = property( fget=lambda self: self.__nMax, fset=lambda self, value: self.Update( nMax=value ) )
    
if __name__ == '__main__':
    plt.clf()
    p = ProgressRing( nMax=50 )
    