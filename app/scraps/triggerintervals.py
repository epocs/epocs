import os
import sys
import glob

import numpy

try: import BCI2000Tools
except ImportError: sys.path.insert( 0, os.path.realpath( os.path.join( os.path.dirname( __file__ ), '..', 'tools', 'python' ) ) )

from BCI2000Tools.FileReader import bcistream, DivertWarnings; DivertWarnings( [] )
from BCI2000Tools.Numerics import events
from BCI2000Tools.Container import Bunch

def Intervals( b ):
	print( b )
	b.seek( 0 )
	signal, states = b.decode()
	if b.params.TriggerState:
		trig = states[ b.params.TriggerState ]
	else:
		trig = signal.A[ b.params.ChannelNames.index( b.params.TriggerChannel ) ] > b.params.TriggerThreshold
	out = b.samples2msec( numpy.diff( events( trig ) ) ) / 1000.0
	print( out )
	summary = Bunch( nTrials=out.size + 1, min=out.min(), max=out.max(), mean=out.mean(), std=out.std() )
	for paramName in 'MinTimeBetweenTriggers MaxRandomTimeBetweenTriggers BackgroundHoldDuration MaxRandomExtraHoldDuration BackgroundSegmentDuration BackgroundFreezeTime SampleBlockSize SamplingRate'.split():
		summary[ paramName ] = b.paramdefs[ paramName ][ 'valstr' ]
	summary[ 'MillisecondsPerSampleBlock' ] = b.samples2msec( b.params.SampleBlockSize )
	print( summary )
	return out

if __name__ == '__main__':
	args = sys.argv[ 1: ]
	
	ind = -1
	try: ind = int( args[ 0 ] )
	except: pass
	else: args[ 0 ] = ''
	
	if not args: args = [ '' ]
	if not args[ 0 ]: args[ 0 ] = os.path.realpath( os.path.join( os.path.dirname( __file__ ), '../../data' ) )
	if args[ 1: ]: args[ 1 ] = int( args[ 1 ] )
	elif os.path.isdir( args[ 0 ] ):
		args[ 0 ] = sorted( ( st.st_mtime, x ) for x in glob.glob( args[ 0 ] + '/**/*.dat', recursive=True ) for st in [ os.stat( x ) ] if st.st_size )[ ind ][ 1 ]
	b = bcistream( *args )
	out = Intervals( b )
	