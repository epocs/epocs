import numpy as np


def spatial_filter(array_type='octagonal', dim=(4, 4, 2), origin=(0., 0.), th=0.):
    # array_type: the array being used
    # dim: the (x, y, z) degrees of the polynomial fit (default x^4 * y^4 * z^2)
    # origin: the (x, y) location, in meters, at which to estimate the gradient (default (0., 0.), center of the array)
    # th: the angle, in radians, at which to estimate the gradient (default 0., pointed along the +x axis)

    # returns a formatted string to be used to configure a spatial filter in EPOCS

    (x, y) = get_contact_locations(array_type)
    c = get_polynomial_coefficients(dim)
    xq = np.array([[origin[0], origin[1]]])

    g = get_direction_matrix(th, xq.shape[0])
    pxy = get_gradient_matrix(xq, c)
    p_inv = get_regression_matrix(x, c)

    z = np.matrix.flatten( np.dot( np.dot( g, pxy ), p_inv ) )
    #z = np.matrix.flatten(g @ pxy @ p_inv)

    weights = []
    for i in range(0, len(z)):
        weights.append((i+1, 1, z[i]))

    weights += y

    n = 1 + len(weights)
    config_str = 'Filtering matrix SpatialFilter=  %.0d { In Out Wt }' % n
    for w in weights:
        config_str += ' acq%02.0d EMG%1.0d %.4f' % w
    config_str += ' TRIG TRIG 1.0'
    return config_str


def get_contact_locations(array_type):
    contact_locations = {
        'octagonal': [.005, .01] * np.array([[7, 7, 5, 5, 5, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -7, -7, -5, -5, -5],
                                             [0, -1, -1, 0, 1, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2, 0, -1, -1, 0, 1]]).transpose(),
        'aztecFishGod': [],
    }

    antagonist_channels = {
        'octagonal': [(31, 2, 100./3), (32, 2, -100./3)],
        'aztecFishGod': [],
    }

    return (np.array(contact_locations[array_type]), antagonist_channels[array_type])


def get_polynomial_coefficients(dim=(4, 4, 2)):
    tmp = []
    for xind in range(0, dim[0]+1):
        for yind in range(0, dim[1]+1):
            for zind in range(0, dim[2]+1):
                if xind+yind+zind <= max(dim):
                    tmp.append([xind, yind, zind])
    exp0 = np.array(tmp).transpose()
    coef0 = np.ones((1, exp0.shape[1]))
    coef1 = coef0 * exp0
    exp1x = exp0.copy()
    exp1x[0, :] = np.maximum(exp0[0, :]-1, 0)
    exp1y = exp0.copy()
    exp1y[1, :] = np.maximum(exp0[1, :] - 1, 0)
    exp1z = exp0.copy()
    exp1z[2, :] = np.maximum(exp0[2, :] - 1, 0)
    coef2 = coef1.copy()
    coef2[0, :] = coef1[0, :] * exp1x[0, :]
    coef2[1, :] = coef1[1, :] * exp1y[1, :]
    coef2[2, :] = coef1[2, :] * exp1z[2, :]
    exp2x = exp1x.copy()
    exp2x[0, :] = np.maximum(exp1x[0, :]-1, 0)
    exp2y = exp1y.copy()
    exp2y[1, :] = np.maximum(exp1y[1, :] - 1, 0)
    exp2z = exp1z.copy()
    exp2z[2, :] = np.maximum(exp1z[2, :] - 1, 0)

    coeff = {
        'exp0': exp0,
        'coef0': coef0,
        'coef1': coef1,
        'exp1x': exp1x,
        'exp1y': exp1y,
        'exp1z': exp1z,
        'coef2': coef2,
        'exp2x': exp2x,
        'exp2y': exp2y,
        'exp2z': exp2z,
    }

    return coeff


def get_direction_matrix(th, ndims):
    g = np.zeros((ndims, 2*ndims))
    for i in range(0, ndims):
        g[i, 2 * i] = np.cos(th)
        g[i, 2 * i + 1] = np.sin(th)
    return g


def get_gradient_matrix(X, c):

    Pxy = np.array([c['coef1'][0, :] * (X[:, 0] ** c['exp1x'][0, :]) * (X[:, 1] ** c['exp1x'][1, :]) * (0 ** c['exp1x'][2, :]),
                    c['coef1'][1, :] * (X[:, 0] ** c['exp1y'][0, :]) * (X[:, 1] ** c['exp1y'][1, :]) * (0 ** c['exp1y'][2, :])])

    return Pxy


def get_regression_matrix(X, c):

    x = X[:, 0].transpose()
    y = c['exp0'][0, :]

    P = (X[:, 0].reshape((-1, 1)) ** c['exp0'][0, :].T) * (X[:, 1].reshape((-1, 1)) ** c['exp0'][1, :].T) * (0. ** c['exp0'][2, :].T)
    # Px2 = c['coef2'][0, :] * (X[:, 0] ** c['exp2x'][0, :]) * (X[:, 1] ** c['exp2x'][1, :]) * (0. ** c['exp2x'][2, :])
    # Py2 = c['coef2'][1, :] * (X[:, 0] ** c['exp2y'][0, :]) * (X[:, 1] ** c['exp2y'][1, :]) * (0. ** c['exp2y'][2, :])
    # Pz2 = c['coef2'][2, :] * (X[:, 0] ** c['exp2z'][0, :]) * (X[:, 1] ** c['exp2z'][1, :]) * (0. ** c['exp2z'][2, :])
    Pinv = np.linalg.pinv(P)

    return Pinv

if __name__ == '__main__':
	import sys, ast
	args = sys.argv[ 1: ]
	origin = ast.literal_eval( args.pop( 0 ) ) if args else ( 0.0, 0.0 )
	angle = ast.literal_eval( args.pop( 0 ) ) if args else 0.0
	print( spatial_filter( origin=origin, th=angle * np.pi / 180.0 ) )
