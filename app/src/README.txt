This is *not* the complete set of C++ sources required to build the BCI2000 binaries
that EPOCS needs.  Rather, this directory just contains the "custom" additions that
are needed on top of a standard BCI2000 source distro, to build EPOCS' custom
signal-processing module, `ReflexConditioningSignalProcessing`.

To use these sources:

1.  Install cmake (select "Add to PATH for all users" during wizard installation)

2.  Install TortoiseSVN (explicitly include the command-line tools during install).

3.  Install Qt

4.  Check out the BCI2000 sources::

        mkdir C:\neurotech\bci2000-svn
        cd C:\neurotech\bci2000-svn
        svn co https://bci2000.org/svn/trunk HEAD

5.  Link the `app\src\custom` directory to your BCI2000 distro, so that it appears
    as `bci2000-svn\HEAD\src\custom`. Alternatively, if you have a pre-existing
	`bci2000-svn\HEAD\src\custom` directory, link the specific project directory
	`app\src\custom\ReflexConditioningSignalProcessing` so that it appears inside
	it (don't forget to update your `bci2000-svn\HEAD\src\custom\CMakeLists.txt` if so).
		
	The `app\build\MakeSrcCustomJunction.cmd` script will perform the necessary
	`mklink /J` command for you if you're linking the whole of `custom` (call it with
	`-h` for help).

5.  Either run `app\build\VisualStudio.cmd` or set the `%VSYEAR%` and `%VSVERSION%`
    environment variables yourself (for example, `SET VSYEAR=2019`, `set VSVERSION=16`)

6,  Change directory to the new `bci2000-svn\HEAD\src\custom` and run `configure.cmd`.
    NB: it assumes Qt 6.2.3, but the default Qt path can be overridden as the (one and
	only) command-line argument to the script.

6.  `start ..\..\build\BCI2000.sln`, switch to Release mode, then build BCI2000.
    For more details, see the tutorial at
	http://doc.bci2000.org/Programming_Howto:Quickstart_Guide

