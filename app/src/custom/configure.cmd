@echo off
set "OLDDIR=%CD%
cd "%~dp0..\..\build"
set "BUILDDIR=%CD%
cd "%OLDDIR%"

if "%VSVERSION%"=="" call VisualStudio
if "%VSVERSION%"=="" (
	set VSVERSION=16
	set VSYEAR=2019
)

:: optional command-line argument
set PREFIXPATH=C:\Qt\6.2.3\msvc%VSYEAR%_64
if not "%~1"=="" set "PREFIXPATH=%~1"
echo.
echo ::::::::::::::::::::::::::::::::::::::::::::::
echo :: CMAKE_PREFIX_PATH will be "%PREFIXPATH%"
if "%~1"=="" echo :: (to change this, delete CMakeCache.txt and then re-run this script
if "%~1"=="" echo :: with the correct directory as the command-line argument)
echo ::::::::::::::::::::::::::::::::::::::::::::::
echo.

@echo on

cmake ^
	-G "Visual Studio %VSVERSION%" -A x64 "-DCMAKE_PREFIX_PATH=%PREFIXPATH%" ^
    -DBUILD_ALL_SOURCEMODULES=ON ^
    -DBUILD_BCPY2000=ON ^
    -DBUILD_COMMANDLINE_FILTERS=ON ^
    -DBUILD_CONTRIB=ON ^
    -DBUILD_TOOLS=ON ^
    -DEXTENSIONS_SERIALINTERFACE=ON ^
    -DBUILD_MINIMIZE_REBUILDS=ON ^
	"-H%BUILDDIR%"  "-B%BUILDDIR%"
	
:: To do an out-of-source build, you can use::
::
::     "-H%BUILDDIR%" "-B%BUILDDIR%\build-qt623-msvc2019_64"
:: 
:: note that the binaries will not end up in their usual places (they will be
:: in a self-contained BCI2000 distro at build\build-qt623-msvc2019_64\BCI2000,
:: which may be missing some items (e.g. parameter files and batch files)
:: relative to the main distro.
