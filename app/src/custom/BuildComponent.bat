:: buildcomponent BCI2000.sln FilePlayback Contrib\SignalSource
set "SLN=%~1
set "TARGET=%~2
set "SECTION=%~3

dir >nul 2>nul
:: resets %ERRORLEVEL% which might otherwise hang over from some previous operation and cause a false failure on the next line

if "%TARGET%"=="" (
	start "" "%SLN%"
	exit /b
)

set "HERE=%~dp0
if "%VSCMD_ARG_TGT_ARCH%"=="" call "%HERE%\VisualStudio.cmd" Win64
msbuild "%SLN%" /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH% || exit /b
