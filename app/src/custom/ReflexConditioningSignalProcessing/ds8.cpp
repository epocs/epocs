#ifndef __INCLUDED_ds8_cpp__
#define __INCLUDED_ds8_cpp__

#include <iostream>
#define REPORT( X )   std::cout << #X << " = " << ( X ) << "\n"

#include "ds8.h"

#include <bitset>
#include <sstream>
#include <Windows.h>

#define PROCTYPE_INIT     DGD128_Initialise
#define PROCNAME_INIT    "DGD128_Initialise"
#define PROCTYPE_UPDATE   DGD128_Update
#define PROCNAME_UPDATE  "DGD128_Update"
#define PROCTYPE_UPDATE2  DGD128_Update
#define PROCNAME_UPDATE2 "DGD128_UpdateEx"
#define PROCTYPE_CLOSE    DGD128_Close
#define PROCNAME_CLOSE   "DGD128_Close"

PROCTYPE_INIT    D128_Init     = NULL;
PROCTYPE_UPDATE  D128_Update   = NULL;
PROCTYPE_UPDATE2 D128_UpdateEx = NULL;
PROCTYPE_CLOSE   D128_Close    = NULL;

#define FAIL( RETURN_VALUE, STUFF )   { std::stringstream _ss; _ss << STUFF; mError = _ss.str(); return RETURN_VALUE; } 
#define CHECK( STUFF ) \
    if( procError   ) FAIL( procError,   ( useAPIVersion == 1 ? PROCNAME_UPDATE : useAPIVersion == 2 ? PROCNAME_UPDATE2 : "???" ) << "() returned error code "   << ( mErrorCode = procError   ) << STUFF ); \
    if( retAPIError ) FAIL( retAPIError, ( useAPIVersion == 1 ? PROCNAME_UPDATE : useAPIVersion == 2 ? PROCNAME_UPDATE2 : "???" ) << "() failed with API error " << ( mErrorCode = retAPIError ) << STUFF );

D128Session::D128Session() : mSessionNumber( 0 ) {}
D128Session::~D128Session() { Close(); }

int
D128Session::Open( void )
{
	if( mSessionNumber ) return mSessionNumber;
	mError = "";
	if( !D128_Init || !D128_Update || !D128_UpdateEx || !D128_Close )
	{
		HINSTANCE dll; 
		const char * dllName = "D128API.DLL";
		if( ( dll = LoadLibraryA( dllName ) ) == NULL ) FAIL( 0, "failed to load " << dllName );
		if( ( D128_Init     = ( PROCTYPE_INIT    )GetProcAddress( dll, PROCNAME_INIT    ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_INIT    << " from " << dllName );
		if( ( D128_Update   = ( PROCTYPE_UPDATE  )GetProcAddress( dll, PROCNAME_UPDATE  ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_UPDATE  << " from " << dllName );
		if( ( D128_UpdateEx = ( PROCTYPE_UPDATE2 )GetProcAddress( dll, PROCNAME_UPDATE2 ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_UPDATE2 << " from " << dllName );
		if( ( D128_Close    = ( PROCTYPE_CLOSE   )GetProcAddress( dll, PROCNAME_CLOSE   ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_CLOSE   << " from " << dllName );
	}
	int procError = 0, retAPIError = 0;
	procError = D128_Init( &mSessionNumber, &retAPIError, NULL, NULL );
	if( procError   ) FAIL( 0, PROCNAME_INIT << "() returned error code "   << procError   );
	if( retAPIError ) FAIL( 0, PROCNAME_INIT << "() failed with API error " << retAPIError );
	return mSessionNumber;
}

void
D128Session::Close( void )
{
	int procError = 0, retAPIError = 0;
	if( mSessionNumber && D128_Close ) procError = D128_Close( &mSessionNumber, &retAPIError, NULL, NULL );
	mSessionNumber = 0;
}

D128Session D128SESSION;

D128Device::D128Device( int deviceIndex, int serialNumber )
: mErrorCode( 0 ), mAPIVersion( -1 ), mAllInfoSize( 0 ), mAllInfo( NULL ), mAllInfoEx( NULL ), mResult( 0 ), mDebug( false )
{
	mAllInfoSize = 2048;
	mAllInfo = ( D128 * )malloc( mAllInfoSize );      // For receiving info about (potentially) multiple stimulators via DGD128_Update()
	mHeader.DeviceCount = 1;       // For sending info about one particular stimulator via DGD128_Update()
	mDesired.D128_DeviceID = 0;    //     (this field will be populated when we SelectDevice(), to target the desired stimulator)

	mAllInfoEx = ( D128EX * )malloc( mAllInfoSize ); // For receiving info about (potentially) multiple stimulators via DGD128_UpdateEx()
	mHeaderEx.DeviceCount = 1;     // For sending info about one particular stimulator via DGD128_UpdateEx()
	mDesiredEx.D128_DeviceID = 0;  //     (this field will be populated when we SelectDevice(), to target the desired stimulator)
	
	mSerialNumber = -1;
	mFirmwareVersion[ 0 ] = mFirmwareVersion[ 1 ] = mFirmwareVersion[ 2 ] = mFirmwareVersion[ 3 ] = -1;
	mAPIVersion = -1;
	
	BlankSlate();
	SelectDevice( deviceIndex, serialNumber );	
}

D128Device::~D128Device() { CleanUp(); }

void
D128Device::CleanUp( void )
{
	if( mAllInfo ) free( mAllInfo );
	mAllInfo = NULL;
	if( mAllInfoEx ) free( mAllInfoEx );
	mAllInfoEx = NULL;
}

int
D128Device::Update( bool send, int useAPIVersion )
{
	int sessionReference = D128SESSION.Open();
	if( !sessionReference ) FAIL( -1, D128SESSION.mError );

	int procError = 0, retAPIError = 0, nBytesToReceive = 0, nBytesToSend = 0;
	void * receiveAddr = NULL;
	void * sendAddr = NULL;
	DGD128_Update updateFunc = NULL;
	
	if( useAPIVersion <= 0 ) useAPIVersion = mAPIVersion;
	if( useAPIVersion == 1 )
	{
		updateFunc = D128_Update;
		receiveAddr = ( void * )mAllInfo;
		if( send )
		{
			sendAddr = ( void * )&mHeader;
			nBytesToSend = ( int )( ( char * )( &mDesired + 1 ) - ( char * )&mHeader );
			PackSlate();
		}
	}
	else if( useAPIVersion == 2 )
	{
		updateFunc = D128_UpdateEx;
		receiveAddr = ( void * )mAllInfoEx;
		if( send )
		{
			sendAddr = ( void * )&mHeaderEx;
			nBytesToSend = ( int )( ( char * )( &mDesiredEx + 1 ) - ( char * )&mHeaderEx );
		}
	}
	else FAIL( -1, "Internal error: have not determined which API version to use for the desired stimulator" );

	procError = updateFunc( sessionReference, &retAPIError, NULL, 0, NULL, &nBytesToReceive, NULL, NULL );	
	CHECK( " while querying size of state information" );		
	if( mDebug )
	{
		REPORT( nBytesToReceive );
		REPORT( nBytesToSend );
	}
	if( nBytesToReceive > mAllInfoSize ) FAIL( -2, "failed to get state: API wants to send " << nBytesToReceive << " but the wrapper only reserved " << mAllInfoSize ); // shouldn't happen now: we just pre-reserve 2KB which should be enough for 30 or 40 devices
	procError = updateFunc( sessionReference, &retAPIError, ( PD128 )sendAddr, nBytesToSend, ( PD128 )receiveAddr, &nBytesToReceive, NULL, NULL );
	CHECK( " while" << ( send ? " sending and " : " " ) << "querying device states" );
	int nDevices = GetNumberOfDevices();
	for( int iDevice = 0; iDevice < nDevices; iDevice++ )
	{
		if( receiveAddr == mAllInfo   &&   mAllInfo->State[ iDevice ].D128_DeviceID == mSerialNumber ) {   mDesired =   mAllInfo->State[ iDevice ]; UnpackSlate(); break; }
		if( receiveAddr == mAllInfoEx && mAllInfoEx->State[ iDevice ].D128_DeviceID == mSerialNumber ) { mDesiredEx = mAllInfoEx->State[ iDevice ]; break; }
	}
	return 0;
}

int
D128Device::SelectDevice( int deviceIndex, int serialNumber )
{
	mAPIVersion = 1; Update( false );  // Use API version 1 initially. The result will therefore be in mAllInfo, not mAllInfoEx
	if( deviceIndex < 0 && serialNumber < 0 ) deviceIndex = 0;
	if( deviceIndex >= mAllInfo->Header.DeviceCount ) FAIL( 0, "failed to find a device at index " << deviceIndex << " (device count = " << mAllInfo->Header.DeviceCount << ")" );
	if( deviceIndex < 0 )
	{
		for( int tryDeviceIndex = 0; tryDeviceIndex < mAllInfo->Header.DeviceCount; tryDeviceIndex++ )
		{
			if( mAllInfo->State[ tryDeviceIndex ].D128_DeviceID == serialNumber )
			{
				deviceIndex = tryDeviceIndex;
				break;
			}
		}
		if( deviceIndex < 0 )
		{
			std::stringstream ss;
			for( int tryDeviceIndex = 0; tryDeviceIndex < mAllInfo->Header.DeviceCount; tryDeviceIndex++ ) ss << ( tryDeviceIndex ? ", " : "" ) << mAllInfo->State[ tryDeviceIndex ].D128_DeviceID;
			FAIL( 0, "failed to find a device with serial number " << serialNumber << " (found " << ss.str() << ")" );
		}
	}
	mSerialNumber = mAllInfo->State[ deviceIndex ].D128_DeviceID;
    unsigned char * firmwareBytes = ( unsigned char * )&mAllInfo->State[ deviceIndex ].D128_VersionID;
	mFirmwareVersion[ 0 ] = firmwareBytes[ 3 ];
	mFirmwareVersion[ 1 ] = firmwareBytes[ 2 ];
	mFirmwareVersion[ 2 ] = firmwareBytes[ 1 ];
	mFirmwareVersion[ 3 ] = firmwareBytes[ 0 ];
	if( mFirmwareVersion[ 0 ] > 1 || ( mFirmwareVersion[ 0 ] == 1 && mFirmwareVersion[ 1 ] >= 4 ) )
		mAPIVersion = 2; // For stimulators with firmware >= 1.4.0.0, henceforth we will use DLL function DGD128_UpdateEx() instead of DGD128_Update()
	mDesired.D128_DeviceID = mSerialNumber;
	mDesiredEx.D128_DeviceID = mSerialNumber;
	Update( false );
	return mSerialNumber;
}

int
D128Device::GetNumberOfDevices( void )
{
	if( mAPIVersion == 1 ) return mAllInfo->Header.DeviceCount;
	if( mAPIVersion == 2 ) return mAllInfoEx->Header.DeviceCount;
	return -1;
}

D128STATEEX &
D128Device::Slate( void )
{
	return mDesiredEx.D128_State;
}

D128STATEEX &
D128Device::BlankSlate( void )
{
	mDesiredEx.D128_VersionID = -1;
	mDesiredEx.D128_Error = -1;
	//mDesiredEx.D128_State.CONTROL.ENABLE = -1;   // despite what the manual says, we cannot set this to -1 (-> beep + warning triangle appears in first status icon position)  (at least true with DGD128_Update, actually unconfirmed with DGD128_UpdateEx)
	//mDesiredEx.D128_State.CONTROL.MODE = -1;     // despite what the manual says, we cannot set this to -1 (-> error code 100013) (at least true with DGD128_Update, actually unconfirmed with DGD128_UpdateEx)
	//mDesiredEx.D128_State.CONTROL.POLARITY = -1; // despite what the manual says, we cannot set this to -1 (-> error code 100013) (at least true with DGD128_Update, actually unconfirmed with DGD128_UpdateEx)
	//mDesiredEx.D128_State.CONTROL.SOURCE = -1;   // despite what the manual says, we cannot set this to -1 (-> error code 100013) (at least true with DGD128_Update, actually unconfirmed with DGD128_UpdateEx)
	mDesiredEx.D128_State.CONTROL.ZERO = -1;
	mDesiredEx.D128_State.CONTROL.TRIGGER = -1;
	mDesiredEx.D128_State.CONTROL.NOBUZZER = -1;
	mDesiredEx.D128_State.CONTROL.RESERVED = 0;
	mDesiredEx.D128_State.DEMAND = -1;
	mDesiredEx.D128_State.LIMIT = -1;
	mDesiredEx.D128_State.WIDTH = -1;
	mDesiredEx.D128_State.RECOVERY = -1;
	mDesiredEx.D128_State.DWELL = -1;
	mDesiredEx.D128_State.CPULSE = 0;
	mDesiredEx.D128_State.COOC = 0;
	mDesiredEx.D128_State.CTOOFAST = 0;
	mDesiredEx.D128_State.FSTATE.OVERENERGY = 0;
	mDesiredEx.D128_State.FSTATE.HWERROR = 0;
	mDesiredEx.D128_State.FSTATE.reserved = 0;
	return mDesiredEx.D128_State;
}

void
D128Device::PackSlate( void )
{
#	undef COPY
#	define COPY( X ) mDesired.X = mDesiredEx.X
	COPY( D128_DeviceID );
	COPY( D128_VersionID );
	COPY( D128_Error );
	COPY( D128_State.CONTROL.ENABLE );
	COPY( D128_State.CONTROL.MODE );
	COPY( D128_State.CONTROL.POLARITY );
	COPY( D128_State.CONTROL.SOURCE );
	COPY( D128_State.CONTROL.ZERO );
	COPY( D128_State.CONTROL.TRIGGER );
	COPY( D128_State.CONTROL.NOBUZZER );
	COPY( D128_State.CONTROL.RESERVED );
	COPY( D128_State.DEMAND );
	//COPY( D128_State.LIMIT ); // does not exist in mDesired
	COPY( D128_State.WIDTH );
	COPY( D128_State.RECOVERY );
	COPY( D128_State.DWELL );
	COPY( D128_State.CPULSE );
	COPY( D128_State.COOC );
	COPY( D128_State.CTOOFAST );
	COPY( D128_State.FSTATE.OVERENERGY );
	COPY( D128_State.FSTATE.HWERROR );
	COPY( D128_State.FSTATE.reserved );
}

void
D128Device::UnpackSlate( void )
{
#	undef COPY
#	define COPY( X ) mDesiredEx.X = mDesired.X
	COPY( D128_DeviceID );
	COPY( D128_VersionID );
	COPY( D128_Error );
	COPY( D128_State.CONTROL.ENABLE );
	COPY( D128_State.CONTROL.MODE );
	COPY( D128_State.CONTROL.POLARITY );
	COPY( D128_State.CONTROL.SOURCE );
	COPY( D128_State.CONTROL.ZERO );
	COPY( D128_State.CONTROL.TRIGGER );
	COPY( D128_State.CONTROL.NOBUZZER );
	COPY( D128_State.CONTROL.RESERVED );
	COPY( D128_State.DEMAND );
	mDesiredEx.D128_State.LIMIT = -1; // does not exist in mDesired
	COPY( D128_State.WIDTH );
	COPY( D128_State.RECOVERY );
	COPY( D128_State.DWELL );
	COPY( D128_State.CPULSE );
	COPY( D128_State.COOC );
	COPY( D128_State.CTOOFAST );
	COPY( D128_State.FSTATE.OVERENERGY );
	COPY( D128_State.FSTATE.HWERROR );
	COPY( D128_State.FSTATE.reserved );
}

const char *
D128Device::Error( void )
{
	return mError.length() ? mError.c_str() : NULL;
}

int
D128Device::ErrorCode( void )
{
	return mErrorCode;
}

const char *
D128Device::ClearError( void )
{
	static std::string clearedError;
	clearedError = mError;
	mError = "";
	mErrorCode = 0;
	return clearedError.length() ? clearedError.c_str() : NULL;
}

int
D128Device::GetResult( void )
{
	return mResult;
}

bool iequals(const std::string& a, const std::string& b)
{
	unsigned int sz = a.size();
	if( b.size() != sz ) return false;
	for( unsigned int i = 0; i < sz; i++ )
		if( std::tolower( a[ i ] ) != std::tolower( b[ i ] ) ) return false;
	return true;
}

int
D128Device::ProcessCommand( const std::string & inputCommand )
{
#	define INTERPRET( FIELD )   if( iequals( cmd, #FIELD ) ) { if( argstr.length() ) Slate().FIELD = arg; else { mResult = Slate().FIELD; resultWasPosted = true; } /* std::cout << #FIELD << " = " << stimulator.Slate().FIELD << std::endl; */ }
	bool resultWasPosted = false;
	int arg = 0;
	std::string cmd, argstr, remainder, bits;
	cmd = inputCommand;
	for( int i = 0; i < cmd.length(); i++ )
	{
		cmd[ i ] = ( cmd[ i ] == '=' ) ? ' '
				 : ( cmd[ i ] == '_' ) ? '.'
				 : std::toupper( cmd[ i ] );
	}

	std::stringstream ss( cmd );
	cmd = "";
	ss >> cmd >> argstr;
	if( cmd == "" ) return 0;
	if( cmd == "UPDATE" ) Update( true );
	else if( cmd == "READ" ) Update( false );
	else
	{
		//if( ss.fail() ) FAIL( -999, "failed to interpret line \"" << ss.str() << "\"" );
		ss >> remainder;
		if( !ss.fail() ) FAIL( -999, "unexpected extra argument \"" << remainder << "\"" );
		std::stringstream ss2( argstr );
		char zero, x;
		bool failed = false;
		if(      argstr.length() > 2 && argstr[ 0 ] == '0' && argstr[ 1 ] == 'X' )
		{
			ss2 >> zero >> x >> std::hex >> arg;
			failed = ss2.fail();
			ss2 >> remainder;
		}
		else if( argstr.length() > 2 && argstr[ 0 ] == '0' && argstr[ 1 ] == 'B' )
		{
			ss2 >> zero >> x >> bits;
			failed = ss2.fail();
			ss2 >> remainder;
			if( !failed )
			{
				try { arg = ( int )std::bitset<32>( bits ).to_ulong(); }
				catch (... ) { failed = true; }
			}
		}
		else if( argstr.length() ) { ss2 >> arg; failed = ss2.fail(); ss2 >> remainder; }
		if( failed || remainder.length() ) FAIL( -999, "failed to interpret value \"" << ss2.str() << "\"" );
		
		if( cmd == "DEMAND!" && !argstr.length() ) cmd = "DEMAND";
		
		if( cmd == "DEMAND!" ) // workaround for some firmware/hardware versions (see note on DEMAND! below)
		{
			Update( true ); // immediately sends any pending updates. Also reads the current state (Update() no longer blanks the slate after it runs)
			if( Slate().CONTROL.ENABLE )
			{
				Slate().CONTROL.ENABLE = 0;
				Slate().DEMAND = arg;
				Update( true );
				::Sleep( 100 );
				Slate().CONTROL.ENABLE = 1;
				Slate().DEMAND = arg;
				Update( true );
			}
			else
			{
				Slate().DEMAND = arg;
				Update( true ); // for consistency, so that you can always assume DEMAND! will conclude with an UPDATE
			}
		}
		else if( cmd == "SLEEP" ) ::Sleep( arg ); // milliseconds
		else if( cmd == "INDEX" ) { if( argstr.length() ) SelectDevice( arg ); }
		else if( cmd == "SERIAL" ) { if( argstr.length() ) SelectDevice( -1, arg ); else { mResult = GetSerialNumber(); resultWasPosted = true; } }
		else INTERPRET( CONTROL.ENABLE   ) // ?   manual says: { 1 = disabled; 2 = enabled; -1 = leave as-is }; but empirically: { 0 = disabled; 1 = enabled; anything else: error icon on stimulator screen } 
		else INTERPRET( CONTROL.MODE     ) // ?A  1 = monophasic; 2 = biphasic
		else INTERPRET( CONTROL.POLARITY ) // ?A  1 = positive;   2 = negative; 3 = alternating
		else INTERPRET( CONTROL.SOURCE   ) // ?   1 = specify DEMAND via panel/API;  2 = specify DEMAND via external analog BNC input
		else INTERPRET( CONTROL.ZERO     ) //     1 = initiates zeroing of the external analog BNC input
		else INTERPRET( CONTROL.TRIGGER  ) //     1 = initiates a trigger (but this is not the most precisely-timed way of doing it: should use a TTL pulse to the trigger BNC instead)
		else INTERPRET( CONTROL.NOBUZZER ) //  A  0 = enable buzzer for out-of-compliance events; 1 = disable buzzer for out-of-compliance events; 
		else INTERPRET( CONTROL.RESERVED ) //     
		else INTERPRET( DEMAND           ) //  A  set this to 10 * (desired current in mA)
		else INTERPRET( LIMIT            ) //     this is 10 * (current limit in mA)
		else INTERPRET( WIDTH            ) //  A  stimulus pulse duration in microseconds (50 to 2000)
		else INTERPRET( RECOVERY         ) //  A  in biphasic mode, specifies the recovery pulse amplitude as a percentage (10 to 100) of the stimulus pulse amplitude
		else INTERPRET( DWELL            ) //  A  time between end of the stimulus pulse and start of recovery pulse in microseconds (1 to 990)
		else INTERPRET( CPULSE           ) //     
		else INTERPRET( COOC             ) //     
		else INTERPRET( CTOOFAST         ) //     
		else INTERPRET( FSTATE.OVERENERGY) //     
		else INTERPRET( FSTATE.HWERROR   ) //     
		else INTERPRET( FSTATE.reserved  ) //     
		else if( cmd == "DEBUG" )
		{
			if( argstr.length() ) mDebug = arg;
			else { mResult = mDebug; resultWasPosted = true; }
		}
		else if( cmd == "REPORT" )
		{
			D128STATEEX & s = Slate();
#			define SREPORT( X )   std::cout << "  " << #X << " = " << ( s.X ) << "\n"
			SREPORT( CONTROL.ENABLE );
			SREPORT( CONTROL.MODE );
			SREPORT( CONTROL.POLARITY );
			SREPORT( CONTROL.SOURCE );
			SREPORT( CONTROL.ZERO );
			SREPORT( CONTROL.TRIGGER );
			SREPORT( CONTROL.NOBUZZER );
			SREPORT( CONTROL.RESERVED );
			SREPORT( DEMAND );
			SREPORT( LIMIT );
			SREPORT( WIDTH );
			SREPORT( RECOVERY );
			SREPORT( DWELL );
			SREPORT( CPULSE );
			SREPORT( COOC );
			SREPORT( CTOOFAST );
			SREPORT( FSTATE.OVERENERGY );
			SREPORT( FSTATE.HWERROR );
			SREPORT( FSTATE.reserved );
			std::cout << std::endl;
		}
		else FAIL( -999, "unrecognized command/field \"" << cmd << "\"" );
	}
	return ( int )resultWasPosted;
}

std::string
D128Device::GetFirmwareVersionString( void )
{
	std::stringstream ss;
	ss << mFirmwareVersion[ 0 ] << "." << mFirmwareVersion[ 1 ] << "." << mFirmwareVersion[ 2 ] << "." << mFirmwareVersion[ 3 ];
	return ss.str();
}

int
D128Device::GetSerialNumber( void )
{
	return mSerialNumber;
}
/*
	infoPtr->Header.DeviceCount                          (int)            //
	infoPtr->State[iDevice].D128_DeviceID                (int)            //     device serial number (presumably use this to verify that you're addressing the same device each time)
	infoPtr->State[iDevice].D128_VersionID               (int)            //     device firmware version
	infoPtr->State[iDevice].D128_Error                   (int)            //     
	infoPtr->State[iDevice].D128_State.CONTROL.ENABLE    (int:2)          // ?   manual says: { 1 = disabled; 2 = enabled; -1 = leave as-is }; but empirically: { 0 = disabled; 1 = enabled; anything else: error icon on stimulator screen } 
	infoPtr->State[iDevice].D128_State.CONTROL.MODE      (int:3)          // ?A  1 = monophasic; 2 = biphasic
	infoPtr->State[iDevice].D128_State.CONTROL.POLARITY  (int:3)          // ?A  1 = positive;   2 = negative; 3 = alternating
	infoPtr->State[iDevice].D128_State.CONTROL.SOURCE    (int:3)          // ?   1 = specify DEMAND via panel/API;  2 = specify DEMAND via external analog BNC input
	infoPtr->State[iDevice].D128_State.CONTROL.ZERO      (int:2)          //     1 = initiates zeroing of the external analog BNC input
	infoPtr->State[iDevice].D128_State.CONTROL.TRIGGER   (int:2)          //     1 = initiates a trigger (but this is not the most precisely-timed way of doing it: should use a TTL pulse to the trigger BNC instead)
	infoPtr->State[iDevice].D128_State.CONTROL.NOBUZZER  (int:2)          //  A  0 = enable buzzer for out-of-compliance events; 1 = disable buzzer for out-of-compliance events; 
	infoPtr->State[iDevice].D128_State.CONTROL.RESERVED  (int:15)         //     
	infoPtr->State[iDevice].D128_State.DEMAND            (int)            //  A  set this to 10 * (desired current in mA)
	infoPtr->State[iDevice].D128_State.LIMIT             (int)            //     10 * (maximal allowed current in mA) - !!!!  NB: this field only exists when working with DGD128_UpdateEx(), not the old DGD128_Update() !!!!
	infoPtr->State[iDevice].D128_State.WIDTH             (int)            //  A  stimulus pulse duration in microseconds (50 to 2000)
	infoPtr->State[iDevice].D128_State.RECOVERY          (int)            //  A  in biphasic mode, specifies the recovery pulse amplitude as a percentage (10 to 100) of the stimulus pulse amplitude
	infoPtr->State[iDevice].D128_State.DWELL             (int)            //  A  time between end of the stimulus pulse and start of recovery pulse in microseconds (1 to 990)
	infoPtr->State[iDevice].D128_State.CPULSE            (unsigned int)   //     number of stimulus pulses delivered since last enabled
	infoPtr->State[iDevice].D128_State.COOC              (unsigned int)   //     number of out-of-complicance events since last enabled
	infoPtr->State[iDevice].D128_State.CTOOFAST          (unsigned int)   //     number of trigger-too-fast events since last enabled
	infoPtr->State[iDevice].D128_State.FSTATE.OVERENERGY (unsigned int:1) //     
	infoPtr->State[iDevice].D128_State.FSTATE.HWERROR    (unsigned int:1) //     
	infoPtr->State[iDevice].D128_State.FSTATE.reserved   (unsigned int:30)//     

	// Fields marked with an "A" are set in Amir's DS8library.cpp implementation
	
	// NB: 
    // - For some firmware/hardware versions, you cannot change DEMAND while CONTROL.ENABLE is on.
    //   You can turn off CONTROL.ENABLE in one update, then simultaneously change DEMAND and turn CONTROL.ENABLE back on in the next update, but you must leave at least 70 msec (maybe longer) pause between successive "enabled" states.
	//   The text command DEMAND! implements this workaround, with a 100ms sleep. Unlike other commands, this implies UPDATE.
	
	// TODO:
	// - You're supposed to be able to set a value of -1 for any of the signed ints in infoPtr->State[iDevice].D128_State and infoPtr->State[iDevice].D128_State.CONTROL
	//   if you mean "no change" but the ones marked '?' don't seem to obey that---see also BlankSlate()
    //   This problem seems to happen with DLL 1.5.0.0 from 2020-11-19, DLL 1.4.3.7 from 2019-07-17  and DLL 1.2.0.0 from 2017-07-08
    //   It might be firmware-version-dependent - the problem happens with stimulator S/N 80, FW 1.4.3.0, but not with the main lab stimulator (whatever version that has)
	// - Note the discrepancy between what the manual says and the actual mapping of values to behavior in CONTROL.ENABLE (doesn't seem API-version-dependent or stimulator-dependent)
	// - Do we need to / how do we obtain an updated D128API.h ?  Probably just fills in a couple of missing error code definitions, nothing more.
    // - Should we just switch to D128Proxy.dll ?   That seems to have a much simpler API.  Does it have the same quirks (above)?
*/

#endif /* __INCLUDED_ds8_cpp__ */
