////////////////////////////////////////////////////////////////////////////////
// Authors:
// Description: DigitimerFilter implementation
// Filter uses the NI board to set an output 
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <iostream>

#include "DigitimerFilter.h"
#include "BCIStream.h"

using namespace std;

RegisterFilter( DigitimerFilter, 2.D );

DigitimerFilter::DigitimerFilter()
: mD188Controller( NULL ), mDS8Controller( NULL ), mDS8Thread( NULL ), mUseWorkaround( false )
{
  
}

DigitimerFilter::~DigitimerFilter()
{
  
}

void
DigitimerFilter::Publish()
{
  BEGIN_PARAMETER_DEFINITIONS

    "Stimulation:Digitimer%20(Deprecated) int   AnalogOutput=              0       0       0      1   // deprecated - must be zero (boolean)", // TODO: remove
    "Stimulation:Digitimer%20(Deprecated) int   EnableDS5ControlFilter=    0       0       0      1   // deprecated - must be zero (boolean)", // TODO: remove
    "Stimulation:Digitimer%20D188         int   EnableD188ControlFilter=   0       0       0      1   // enable D188 control? (boolean)",
    "Stimulation:Digitimer%20D188         int   D188Channel=               1       1       1      8   // Initial channel to use if D188 enabled",
    "Stimulation:Digitimer%20DS8R         int   EnableDS8ControlFilter=    0       0       0      1   // enable DS8 control? (boolean)",
    //"Stimulation:Digitimer%20DS8R         int   UseDS8DemandWorkaround=    0       0       0      1   // (boolean)",
    "Stimulation:Digitimer%20DS8R         int   StimulatorSerialNumber=   -1      -1       %      %   // DS8R Serial number (-1 for auto)",
    "Stimulation:Digitimer%20DS8R         int   StimulationType=           1       1       1      2   // Stimulation waveform type, 1: Monophasic, 2: Biphasic (enumeration)",
    "Stimulation:Digitimer%20DS8R         int   StimulationPolarity=       1       1       1      2   // Stimulation polarity, 1: Positive, 2: Negative (enumeration)",
    "Stimulation:Digitimer%20DS8R         float PulseWidth=                0.5ms   0.5ms   0      %   // Width of the stimulation pulse",
    "Stimulation:Digitimer%20DS8R         int   RecoveryPhaseRatio=      100     100      10    100   // Percentage ratio of recovery-phase to stimulation-phase amplitude, for biphasic stimulation pulses",
    "Stimulation:Digitimer%20DS8R         float InterphaseInterval=        1us     1us     1us  990us // Delay between stimulation and recovery phases of biphasic stimulation pulses",
    "Stimulation:Digitimer%20DS8R         float InitialCurrent=            0       1       0      %   // Initial current to use for stimulation in mA",
    "Stimulation:Digitimer%20DS8R         float MaximumCurrent=          100mA   100mA     0      %   // Current limit (if the stimulator supports this)",
  
  END_PARAMETER_DEFINITIONS
  
  BEGIN_STATE_DEFINITIONS
    // Control the stimulator from outside using these two parameters
    "DesiredMicroamps              18 0 0 0", // Value represented as uA so 16-bit would be 0-65535uA (or 65.535mA max) - not always enough
    "NeedsUpdating                  1 0 0 0", // Set this to 1 when you want to switch to the current DesiredMicroamps value

    // The following states are realtime readouts of the stimulator's status (whether changed by the API or by manipulating physical buttons and dials)
    "StimulatorArmed                1 0 0 0", // 0 for disarmed, 1 for armed despite what the documentation says
    "StimulationPhases              2 0 0 0", // 1 for mono, 2 for bi
    "StimulationPolarity            2 0 0 0", // 1 for positive, 2 for negative, 3 for alternating
    "StimulationPulseWidth         12 0 0 0", // in microseconds
    "StimulationRecoveryPercentage  7 0 0 0", // in percent
    "StimulationInterphaseInterval 11 0 0 0", // in microseconds
    "CurrentAmplitude              18 0 0 0", // in microAmps
  END_STATE_DEFINITIONS
}


#define STIMULATOR_COMMAND( STUFF ) { std::stringstream _ss; _ss << STUFF; std::string _s = _ss.str(); if (mDS8Controller->ProcessCommand(_s) < 0 || mDS8Controller->Error()) { bcierr << "Problem with DS8 stimulator: " << mDS8Controller->Error(); return; } }

void
DigitimerFilter::ConfigureStimulator( int serialNumber, bool sendParameters, int numberOfPhases, int polarity, int pulseWidthInMicroseconds, int recoveryPercentage, int interphaseIntervalInMicroseconds, int demandValue, int limitValue ) const
{
  if( !mDS8Controller ) mDS8Controller = new D128Device( -1, serialNumber ); // we can do this because mDS8Controller is mutable
  if( mDS8Controller->Error() )
  {
    bcierr << "Problem with Digitimer DS8 connection (Error:\"" << mDS8Controller->Error() << "\"). Make sure the DS8 is on, and the DS8 software has been installed." << endl;
    delete mDS8Controller;
    mDS8Controller = NULL;
  }
  else if( sendParameters )
  {
    STIMULATOR_COMMAND( "READ" );
    STIMULATOR_COMMAND( "CONTROL.SOURCE 1" );
    STIMULATOR_COMMAND( "CONTROL.NOBUZZER 0" );
    if( numberOfPhases >= 0 ) STIMULATOR_COMMAND( "CONTROL.MODE "     << numberOfPhases ); // need to check >=0 explicitly, after READ, because the stimulator can go into an error state when you actually send -1 for this value
    if( polarity       >= 0 ) STIMULATOR_COMMAND( "CONTROL.POLARITY " << polarity       ); // need to check >=0 explicitly, after READ, because the stimulator can go into an error state when you actually send -1 for this value
    // for other parameters, -1 seems to be successfully interpreted as "no change"
    STIMULATOR_COMMAND( "WIDTH " << pulseWidthInMicroseconds );
    STIMULATOR_COMMAND( "RECOVERY " << recoveryPercentage );
    STIMULATOR_COMMAND( "DWELL " << interphaseIntervalInMicroseconds );
    if( limitValue >= 0 ) STIMULATOR_COMMAND( "LIMIT " << limitValue ); // need to check >=0 explicitly, because the stimulator does not recognize -1 as "no change" for this value
    if( demandValue >= 0 )
    {
      if (mUseWorkaround)
      {
        STIMULATOR_COMMAND("DEMAND! " << demandValue); // DEMAND! implements the workaround for stimulator models that can't change DEMAND while armed, and concludes with an UPDATE  // <- should be unnecessary now
      }
      else
      {
        STIMULATOR_COMMAND("DEMAND " << demandValue);
        STIMULATOR_COMMAND("UPDATE");
      }
      mLastConfiguredDemand = demandValue;
    }
    else
    {
      STIMULATOR_COMMAND( "UPDATE" );
    }
  }
}

void
DigitimerFilter::Preflight( const SignalProperties& inputProperties, SignalProperties& outputProperties ) const
{
  outputProperties = inputProperties; // this simply passes information through about SampleBlock dimensions, etc...
  
  if( Parameter( "AnalogOutput" ) ) bcierr << "AnalogOutput no longer supported - the parameter value must be 0." << endl;
  if( Parameter( "EnableDS5ControlFilter" ) ) bcierr << "The Digitimer DS5 is no longer supported. EnableDS5Controller must be 0." << endl;
  
  if( Parameter( "EnableDS8ControlFilter" ) )
  {      
    State("DesiredMicroamps");
    State("NeedsUpdating");

    State("StimulatorArmed");               // 0 for disarmed, 1 for armed despite what the documentation says
    State("StimulationPhases");             // 1 for mono, 2 for bi
    State("StimulationPolarity");           // 1 for positive, 2 for negative, 3 for alternating
    State("StimulationPulseWidth");         // in microseconds
    State("StimulationRecoveryPercentage"); // in percent
    State("StimulationInterphaseInterval"); // in microseconds
    State("CurrentAmplitude");              // in microAmps

    //Parameter("UseDS8DemandWorkaround");
      
    double initialCurrentInMilliamps = Parameter("InitialCurrent").In("mA");
    // Wrongheaded units (mV, ms) will generate an error in the .In("mA") call.
    // So the only thing we need to worry about now is a bare unitless number:
    // that is interpreted by the BCI2000 framework as the base unit ("A")
    // regardless of whether you say .In("mA") or .In("A") here.  However, the
    // historical convention (established by the parameter comment/tooltip)
    // is that a bare number means mA here. So:
    PhysicalUnit current; current.SetSymbol("A");
    if (!current.IsPhysical(Parameter("InitialCurrent"))) // this method returns false for wrongheaded units and for bare numbers, but since we've already
      initialCurrentInMilliamps /= 1000.0;                // dealt with the former, we can assume we're dealing with a bare number and correct the gain here
    int demandValue = ( int )( 0.5 + initialCurrentInMilliamps * 10.0 );
    
    double maximumCurrentInMilliamps = Parameter("MaximumCurrent").In("mA");
    if (!current.IsPhysical(Parameter("MaximumCurrent"))) maximumCurrentInMilliamps /= 1000.0;
    int limitValue = ( int )( 0.5 + maximumCurrentInMilliamps * 10.0 );

    int numberOfPhases = Parameter( "StimulationType" ); // 1=monophasic, 2=biphasic
    int polarity = Parameter( "StimulationPolarity" ); // 1=positive, 2=negative, 3=alternating
    int pulseWidthInMicroseconds = ( int )( 0.5 + Parameter( "PulseWidth" ).InMilliseconds() * 1000.0 );
    if( pulseWidthInMicroseconds < 50 || pulseWidthInMicroseconds > 2000 ) bciwarn << "Warning: Pulse Width is " << pulseWidthInMicroseconds << " microseconds, which is outside the 50-2000 range used for H-reflex conditioning" << std::endl;
    int recoveryPercentage = ( int )( 0.5 + Parameter( "RecoveryPhaseRatio" ) );
    int interphaseIntervalInMicroseconds = ( int )( 0.5 + Parameter( "InterphaseInterval" ).InMilliseconds() * 1000.0 );
    ConfigureStimulator( Parameter("StimulatorSerialNumber"), false );
    
  }
    
  if( Parameter( "EnableD188ControlFilter" ) )
  {  
    D188library::D188Functions d188Controller;
    if( d188Controller.ErrorCode ) bcierr << "Unable to initialize Digitimer D188 connection. Make sure the D188 is on, and the D188 software has been installed." << endl;
    int d188Channel = Parameter( "D188Channel" );
    //if( d188Channel < 1 || d188Channel > 8 ) bcierr << "Initial channel set to " << d188Channel << ". Must be an integer from 1 to 8" << endl;
  }
  
}

void
DigitimerFilter::Initialize( const SignalProperties& inputProperties, const SignalProperties& outputProperties )
{
  mUseWorkaround = false; //Parameter("UseDS8DemandWorkaround");

  if( Parameter( "EnableDS8ControlFilter" ) )
  {    
    mDS8SerialNumber = Parameter( "StimulatorSerialNumber" );
    double x;
    PhysicalUnit current; current.SetSymbol("A");
    int numberOfPhases = Parameter( "StimulationType" );
    int polarity = Parameter( "StimulationPolarity" );
    x = Parameter( "PulseWidth" ).InMilliseconds() * 1000; 
    int pulseWidthInMicroseconds = ( int )( 0.5 + x );
    int recoveryPercentage = Parameter( "RecoveryPhaseRatio" );
    x = Parameter( "InterphaseInterval" ).InMilliseconds() * 1000; 
    int interphaseIntervalInMicroseconds = ( int )( 0.5 + x );
    x = Parameter("InitialCurrent").In("mA");
    if (!current.IsPhysical(Parameter("InitialCurrent"))) x /= 1000.0; // see comments in Preflight()
    int demandValue = ( int )( 0.5 + x * 10.0 ); // in multiples of 100uA
    x = Parameter("MaximumCurrent").In("mA");
    if (!current.IsPhysical(Parameter("MaximumCurrent"))) x /= 1000.0; // see comments in Preflight()
    int limitValue = ( int )( 0.5 + x * 10.0 ); // in multiples of 100uA
    ConfigureStimulator( mDS8SerialNumber, true, numberOfPhases, polarity, pulseWidthInMicroseconds, recoveryPercentage, interphaseIntervalInMicroseconds, demandValue, limitValue );
  }
  if( Parameter( "EnableD188ControlFilter" ) )
  {
    mD188Controller = new D188library::D188Functions;
    if( !mD188Controller || mD188Controller->ErrorCode ) throw bcierr << "failed to initialize D188 library";
    mD188Controller->SetChannel( Parameter( "D188Channel" ) );
  }
    
}

void
DigitimerFilter::Halt()
{
  delete mDS8Controller;  mDS8Controller  = NULL; D128SESSION.Close();
  delete mD188Controller; mD188Controller = NULL;
  if( mDS8Thread ) { mAbortThread = true; mDS8Thread->join(); delete mDS8Thread; mDS8Thread = NULL; }
}

void
DigitimerFilter::StartRun()
{
  if( mDS8Controller )
  {
    STIMULATOR_COMMAND("CONTROL.ENABLE 1"); // arm the stimulator
    STIMULATOR_COMMAND("UPDATE");
    if( mDS8Thread ) { mAbortThread = true; mDS8Thread->join(); delete mDS8Thread; mDS8Thread = NULL; }
    mAbortThread = false;
    mNewCurrentRequested = false;
    mDS8Thread = new std::thread( &DigitimerFilter::BackgroundThread, this );
  }
  State( "NeedsUpdating" ) = 0;
}

void
DigitimerFilter::StopRun()
{
  if( mDS8Controller )
  {
    STIMULATOR_COMMAND( "CONTROL.ENABLE 0" ); // disarm the stimulator
    STIMULATOR_COMMAND("UPDATE");
    if( mDS8Thread ) { mAbortThread = true; mDS8Thread->join(); delete mDS8Thread; mDS8Thread = NULL; }
  }
}

void
DigitimerFilter::Process( const GenericSignal& inputSignal, GenericSignal& outputSignal )
{
  outputSignal = inputSignal; // Pass the signal through unmodified.
  
  if (mDS8Controller && !mDS8Controller->Error())
  {
    mDesiredDemand = (0.5 + State("DesiredMicroamps") / 100.0); // divide by 100 is correct: demand is expressed in multiples of 100uA
    if (State("NeedsUpdating"))
    {
      if( mNewCurrentRequested ) State("NeedsUpdating") = 0;
      else mNewCurrentRequested = true;
    }

    D128STATEEX & stimulatorState = mDS8Controller->Slate();
#   define STIMULATOR_READOUT(stateName, value) { int _x = (value);  if( _x >= 0 ) State( stateName ) = _x; }
    STIMULATOR_READOUT("StimulatorArmed", stimulatorState.CONTROL.ENABLE);         // 0 for disarmed, 1 for armed despite what the documentation says
    STIMULATOR_READOUT("StimulationPhases", stimulatorState.CONTROL.MODE);         // 1 for mono, 2 for bi
    STIMULATOR_READOUT("StimulationPolarity",  stimulatorState.CONTROL.POLARITY);  // 1 for positive, 2 for negative, 3 for alternating
    STIMULATOR_READOUT("StimulationPulseWidth", stimulatorState.WIDTH);            // in microseconds
    STIMULATOR_READOUT("StimulationRecoveryPercentage", stimulatorState.RECOVERY); // in percent
    STIMULATOR_READOUT("StimulationInterphaseInterval", stimulatorState.DWELL);    // in microseconds
    STIMULATOR_READOUT("CurrentAmplitude", stimulatorState.DEMAND * 100);          // in microAmps (remember DEMAND itself is expressed in multiples of 100uA)
    // The legacy name "CurrentAmplitude" is used for backward compatibility of offline analysis
  }
}

void
DigitimerFilter::BackgroundThread( void )
{
  static int sSleepsSinceLastCommand = 0;
  int sleepMsec = 25;
  while( true )
  {
    while (!mAbortThread && !( mDS8Controller && mDS8Controller->Error()))
    {
      if( ( mNewCurrentRequested || !mDS8Controller ) && sSleepsSinceLastCommand >= 2 )
      {
        ConfigureStimulator(mDS8SerialNumber, true, -1, -1, -1, -1, -1, mDesiredDemand, -1);
        mNewCurrentRequested = false;
        sSleepsSinceLastCommand = 0;
      }
      else if( sSleepsSinceLastCommand >= 4 )
      {
        mDS8Controller->Update(false);
        sSleepsSinceLastCommand = 0;
      }
      ::Sleep( sleepMsec );
      sSleepsSinceLastCommand++;
    }
    if( mAbortThread ) return;
    if( mDS8Controller )
    {
      if( mDS8Controller->ErrorCode() == 109 ) { bciwarn << "Communication pipeline with DS8R stimulator has failed (" << mDS8Controller->Error() << "). Restarting it..."; delete mDS8Controller; mDS8Controller = NULL; ::Sleep( 100 ); }
      else if (mDS8Controller->Error()) { bcierr << "Problem with DS8 stimulator: " << mDS8Controller->Error(); return; }
    }
  }
}
