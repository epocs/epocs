// D188Library.cpp : Defines the exported functions for the DLL application.
//


#include "D188library.h"
#include "stdlib.h"

namespace D188library
{

	D188Functions::D188Functions(void)
	{
		ErrorCode = 0;
		MODE = 1; //USB CONTROL
		CHANNEL = 1; //Channel1
		INDICATOR = 1; //ON
		DELAY = 1; //100us

		if (loadD188APILibrary()) {
			apiRef = 0;
			retError = procInitialise(&apiRef, &retAPIError, NULL, NULL);
			if ((retError != ERROR_SUCCESS) || (retAPIError != ERROR_SUCCESS)) {
				ErrorCode = -1;
			}
			else
			{
				ErrorCode = GetState();
				if (nD188 > 0) {	SetState(); }
				else {ErrorCode = -1;}
			}
		}
		else {
			ErrorCode = -1;
		}
	}

	D188Functions::~D188Functions(void)
	{
		CloseD188();
	}

	bool D188Functions::loadD188APILibrary(void)
	{

		libD188API = LoadLibraryA("DGD188API.DLL");
		if (libD188API) {
			if (!(procInitialise = (DGD188_Initialise)GetProcAddress(libD188API, "DGD188_Initialise"))) {
				//printf("Failed! GetProcAddress(""DGD188_Initialise"")");
				return FALSE;
			};

			if (!(procUpdate = (DGD188_Update)GetProcAddress(libD188API, "DGD188_Update"))) {
				//printf("Failed! GetProcAddress(""DGD188_Update"")");
				return FALSE;
			};

			if (!(procClose = (DGD188_Close)GetProcAddress(libD188API, "DGD188_Close"))) {
				//printf("Failed! GetProcAddress(""DGD188_Close"")");
				return FALSE;
			};

			return TRUE;

		}
		else {
			//printf("Failed to load DGD188API.DLL.\n\r");
			return FALSE;
		}

	}

	int D188Functions::GetState()
	{
	//Get SIZEOF current state (bytes) to be used for CurrentState
		retError = procUpdate(apiRef, &retAPIError, NULL, 0, NULL, &cbState, NULL, NULL);

		if ((retError == ERROR_SUCCESS) && (retAPIError == ERROR_SUCCESS)) {
			//allocate sufficient memory to retreive the device state data
			CurrentState = (PD188)malloc(cbState);
			retError = procUpdate(apiRef, &retAPIError, NULL, 0, CurrentState, &cbState, NULL, NULL);
			if ((retError == ERROR_SUCCESS) && (retAPIError == ERROR_SUCCESS)) {
				nD188 = CurrentState->Header.DeviceCount;
			}
			else { return -1; }
		}
		else
		{
			return -1;
		}

		return 0;
	}

	int D188Functions::SetState()
	{
		CurrentState->State[0].D188_State.D188_Mode = MODE;
		CurrentState->State[0].D188_State.D188_Indicator = INDICATOR;
		CurrentState->State[0].D188_State.D188_Delay = DELAY;
		CurrentState->State[0].D188_State.D188_Select = 1 << CHANNEL;

		retError = procUpdate(apiRef, &retAPIError, CurrentState, cbState, CurrentState, &cbState, NULL, NULL);

		if ((retError != ERROR_SUCCESS) || (retAPIError != ERROR_SUCCESS)) {
			if (retError != ERROR_SUCCESS) { return retError; }
			else if (retAPIError != ERROR_SUCCESS) { return retAPIError; }
		}
		else {
			return 0;
		}

		return 0;
	}

	void D188Functions::SetChannel(int channel)
	{
		/*
		D188_Select
		0 = All channels off
		1 = Channel 1 selected
		2 = Channel 2 selected
		4 = Channel 3 selected
		8 = Channel 4 selected
		16 = Channel 5 selected
		32 = Channel 6 selected
		64 = Channel 7 selected
		128 = Channel 8 selected
		*/

		if (channel == 0){ 
			CHANNEL = 0;
		}
		else { 
			CHANNEL = channel-1;
		}
		SetState();
	}

	void D188Functions::CloseD188()
	{
		if (ErrorCode == 0){
			if (apiRef) {
				SetChannel(0);
				retError = procClose(&apiRef, &retAPIError, NULL, NULL);
			}
		}
	}




}