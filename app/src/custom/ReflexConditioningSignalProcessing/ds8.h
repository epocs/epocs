#ifndef __INCLUDED_ds8_h__
#define __INCLUDED_ds8_h__

#include <string>

#include "D128API.h"

class D128Session
{
	private:
		int         mSessionNumber;
	
	public:
		std::string mError;
	
		D128Session();
		~D128Session();
		int Open( void );
		void Close( void );
};
extern D128Session D128SESSION;

class D128Device
{
	private:
	
		DEVHDR             mHeader;      // these two members must be
		D128DEVICESTATE    mDesired;     // declared together in this order
	
		DEVHDR             mHeaderEx;   // these two members must be
		D128DEVICESTATEEX  mDesiredEx;  // declared together in this order
 
		std::string mError;
		int         mErrorCode;

		int         mResult;
		int         mAllInfoSize;
		D128 *      mAllInfo;
		D128EX *    mAllInfoEx;
		bool        mDebug;
		int         mSerialNumber;
		int         mFirmwareVersion[ 4 ];
		int         mAPIVersion;  // <=0 means unknown for this stimulator, 1 means use DGD128_Update(), 2 means use DGD128_UpdateEx()
	
	public:
		D128Device( int deviceIndex=-1, int serialNumber=-1 );
		~D128Device();
		void CleanUp( void );
		int Update( bool send=false, int useAPIVersion=-1 );
		int SelectDevice( int deviceIndex=0, int serialNumber=-1 );
		int GetNumberOfDevices( void );
		D128STATEEX & Slate( void );
		D128STATEEX & BlankSlate( void );
		void PackSlate( void );
		void UnpackSlate( void );
		const char * Error( void );
		int ErrorCode( void );
		const char * ClearError( void );
		int GetResult( void );
		int ProcessCommand( const std::string & inputCommand );
		int GetSerialNumber( void );
		std::string GetFirmwareVersionString( void );
};

#endif /* __INCLUDED_ds8_h__ */
