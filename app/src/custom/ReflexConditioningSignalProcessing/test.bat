#!../../../prog/BCI2000Shell
@cls & ..\..\..\prog\BCI2000Shell %0 %* #! && exit /b 0 || exit /b 1

system taskkill /F /FI "IMAGENAME eq NIDAQ_mx_Source.exe"
system taskkill /F /FI "IMAGENAME eq SignalGenerator.exe"
system taskkill /F /FI "IMAGENAME eq ReflexConditioningSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"

system taskkill /F /FI "IMAGENAME eq NIDAQ_~1.exe"
system taskkill /F /FI "IMAGENAME eq SIGNAL~1.exe"
system taskkill /F /FI "IMAGENAME eq REFLEX~1.exe"
system taskkill /F /FI "IMAGENAME eq DUMMYA~1.exe"

change directory $BCI2000LAUNCHDIR

show window
set title ${extract file base $0}
reset system
startup system localhost

start executable SignalGenerator                    --local --FileFormat=null
start executable ReflexConditioningSignalProcessing --local --NumberOfThreads=1
start executable DummyApplication                   --local

wait for connected

set parameter Blah int    SourceCh=               3
set parameter Blah list   ChannelNames=           3 EMG1 EMG2 TRIG
set parameter Blah list   ChannelsToTrap=         2 EMG1 EMG2
set parameter Blah string TriggerChannel=           TRIG
set parameter Blah float  SamplingRate=             3200Hz
set parameter Blah int    SampleBlockSize=          128

set parameter Blah int    EnableDS8ControlFilter=   1
set parameter Blah int    InitialCurrent=           2.4mA


visualize watch StimulatorArmed
visualize watch StimulationPhases
visualize watch StimulationPolarity
visualize watch StimulationPulseWidth
visualize watch StimulationRecoveryPercentage
visualize watch StimulationInterphaseInterval
visualize watch CurrentAmplitude

#set script OnSuspend "quit"
setconfig
set state Running 1
