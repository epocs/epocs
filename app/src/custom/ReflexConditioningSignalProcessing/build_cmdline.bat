@echo off

set "OLDD=%CD%
set "HERE=%~dp0
cd /D "%HERE%"
if not "%VSCMD_ARG_TGT_ARCH%"=="x64" call "%HERE%\..\VisualStudio.cmd" Win64

:: must be run from within the BCI2000 tree
:: command-line argument could be e.g.  C:\neurotech\epocs\app

:: NB: if CMakeCache.txt is newer than mid-2021, do we need to transfer the vcredist DLLs as well?
::     see outputs of c:\neurotech\bci2000-svn\HEAD\build\build_utils\list_dependencies.exe with and without --redist
::     (/app/build/CopyBCI2000Components.cmd does this, but the current script does not)

set  SECTION=Core\Tools\cmdline
set LOCATION=tools\cmdline
:: stream tools (core)
set   TARGET=bci_dat2stream
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=bci_stream2mat
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=bci_stream2hybrid
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
:: filter tools (core)
set   TARGET=TransmissionFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=SpatialFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=IIRBandpass
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\

:: filter tools (custom)
set  SECTION=Custom
set LOCATION=tools\cmdline
set   TARGET=BackgroundTriggerFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=TrapFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=RangeIntegrator
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\

cd /D "%OLDD%"
