////////////////////////////////////////////////////////////////////////////////
// Authors: 
// Description: DigitimerFilter header
////////////////////////////////////////////////////////////////////////////////

#ifndef INCLUDED_DIGITIMERFILTER_H  // makes sure this header is not included more than once
#define INCLUDED_DIGITIMERFILTER_H

#include "GenericFilter.h"
#include "D188Library.h"
#include "ds8.h"

#include <thread>

class DigitimerFilter : public GenericFilter
{
  public:
    DigitimerFilter();
    ~DigitimerFilter();
    void Publish() override;
    void Halt() override;
    void ConfigureStimulator(
      int serialNumber,
      bool sendParameters=false,
      int numberOfPhases=-1, int polarity=-1,
      int pulseWidthInMicroseconds=-1, int recoveryPercentage=-1, int interphaseIntervalInMicroseconds=-1,
      int demandValue=-1, int limitValue=-1
    ) const;
    void Preflight( const SignalProperties& inputProperties, SignalProperties& outputProperties ) const override;
    void Initialize( const SignalProperties& inputProperties, const SignalProperties& outputProperties ) override;
    void StartRun() override;
    void StopRun() override;
    void Process( const GenericSignal& inputSignal, GenericSignal& outputSignal ) override;
    
  private:
    D188library::D188Functions * mD188Controller; // from Amir's D188Library.*
    int  mD188Channel;
  
    mutable D128Device * mDS8Controller;  // from Jez's ds8.*
    std::thread * mDS8Thread;  
    int mDesiredDemand;
    int mDS8SerialNumber;
    mutable int mLastConfiguredDemand;
    bool mUseWorkaround;
    bool mAbortThread;
    bool mNewCurrentRequested;
    void BackgroundThread( void );
};

#endif // INCLUDED_DIGITIMERFILTER_H