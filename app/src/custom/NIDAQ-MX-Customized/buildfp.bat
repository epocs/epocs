@echo off

set "OLDD=%CD%
set "HERE=%~dp0
cd /D "%HERE%"
if not "%VSCMD_ARG_TGT_ARCH%"=="x64" call "%HERE%\..\VisualStudio.cmd" Win64

:: must be run from within the BCI2000 tree
:: command-line argument could be e.g.  C:\neurotech\epocs\app

set  SECTION=Contrib\SignalSource
set LOCATION=prog
set   TARGET=FilePlayback

msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
:: NB: if CMakeCache.txt is newer than mid-2021, we'll need to transfer the vcredist DLLs as well
::     see outputs of c:\neurotech\bci2000-svn\HEAD\build\build_utils\list_dependencies.exe with and without --redist
::     (/app/build/CopyBCI2000Components.cmd does this, but the current script does not)

cd /D "%OLDD%"
