__all__ = [
	'DS8R', # the CONSTANTS will also get automatically unpacked into this list
]

class CONSTANTS:
	
	# control_enable values:
	OFF = ENABLED  = 0
	ON  = DISABLED = 1
	
	# control_mode values:
	MONOPHASIC = 1
	BIPHASIC   = 2

	# control_polarity values:
	POSITIVE    = 1
	NEGATIVE    = 2
	ALTERNATING = 3

	# control_source values:
	INTERNAL = 1
	EXTERNAL = 2

	# control_nobuzzer values:
	AUDIBLE = 0
	SILENT  = 1

for name, value in CONSTANTS.__dict__.items():
	if name.startswith( '_' ): continue
	globals()[ name ] = value
	__all__.append( name )


import os
import sys
import threading
import subprocess


HERE = os.path.dirname( __file__ )

if sys.version_info[ 0 ] < 3: bytes = str
else: unicode = str
	
class DigitimerException( Exception ): pass

def DS8R( *pargs, **kwargs ):
	"""
	Ordered arguments `*pargs`, if supplied, are taken first: each
	one should be a string expressing a command of the form
	`'KEY VALUE'`. The one exception is the `'UPDATE'` command which
	has no `VALUE`.
	
	Keyword arguments `**kwargs` are a more-convenient interface
	to the same command set, but you cannot guarantee the order in
	which they are carried out. Keys are case-insensitive, and
	underscores can be used instead of dots.
	
	The commands are formatted and piped to `ds8.exe` which should
	reside in the same directory as this Python file.
	
	Example::
	
	    from ds8 import *
	    DS8R( demand=26, control_mode=BIPHASIC, control_polarity=POSITIVE, control_source=INTERNAL, control_enable=ON )
	
	The underlying command set is as follows::

	    CONTROL.ENABLE     the manual says: { 1 = disabled; 2 = enabled; -1 = leave as-is }; however, empirically it is: { 0 = disabled; 1 = enabled; anything else: error icon on stimulator screen } 
	    CONTROL.MODE       1 = monophasic; 2 = biphasic
	    CONTROL.POLARITY   1 = positive;   2 = negative; 3 = alternating
	    CONTROL.SOURCE     1 = specify DEMAND via panel/API;  2 = specify DEMAND via external analog BNC input
	    CONTROL.ZERO       1 = initiates zeroing of the external analog BNC input
	    CONTROL.TRIGGER    1 = initiates a trigger (but note that this is not the most precisely-timed way of doing it: should use a TTL pulse to the trigger BNC instead)
	    CONTROL.NOBUZZER   0 = enable buzzer for out-of-compliance events; 1 = silence the buzzer for out-of-compliance events; 
	    DEMAND             set this to 10 * (desired current in mA)
	    WIDTH              stimulus pulse duration in microseconds (50 to 2000)
	    RECOVERY           in biphasic mode, specifies the recovery pulse amplitude as a percentage (10 to 100) of the stimulus pulse amplitude
	    DWELL              in biphasic mode, specifies the time between end of the stimulus pulse and start of recovery pulse in microseconds (1 to 990)

	This function automatically appends an UPDATE command at the end
	of your complete set of commands (ordered and keyword args, taken
	together).

	Special keyword arguments (not translated directly into commands
	or passed through to `ds8.exe`), and their default values, are as
	follows:
	
	`threaded=True`:
	    If true, use a background thread to launch the `ds8.exe`
	    subprocess. The `DS8R` function will return immediately (but
	    the thread will probably take 400-600msec to actually do
	    its job, and you should not call this function again
	    until it has. With `threaded=False` the `DS8R` function
	    will wait synchronously for `ds8.exe` to finish.
	    
	`workaround=False`:
	    On some stimulators but not others (and I'm not sure whether
	    this is dependent on hardware or firmware version), you cannot
	    change DEMAND while the stimulator is enabled. So, to work 
	    around this::
	    
	        DS8R( demand=25, workaround=True )
	    
	    gets automatically expanded, under the hood, into::
	    
	        DS8R(
	            'CONTROL.ENABLE 0',
	            'UPDATE',
	            'SLEEP 100',  # must leave at least 90ms pause between successive CONTROL.ENABLED=1 states
	            'DEMAND 25',  # or 10 * whatever number of mA you want
	            'CONTROL.ENABLE 1',
	        )
	
	`output=None`:
	    If this is a list, when the command ends the list will be
	    populated with the lines output by `ds8.exe`
	
	`debug=False`:
	    If True, print the commands before they get sent to `ds8.exe`.
	
	"""
	threaded = kwargs.pop( 'threaded', True )
	if threaded:
		kwargs[ 'threaded' ] = False
		threading.Thread( target=DS8R, args=pargs, kwargs=kwargs ).start()
		return
	
	debug = kwargs.pop( 'debug', False )

	output = kwargs.pop( 'output', None )
	if output is None: output = []

	finalKwargs = []
	workaround = kwargs.pop( 'workaround', False )	
	if workaround:
		# convert the `demand` keyword argument, if any, into a `DEMAND!` command, and make sure it comes last (because it expands inside ds8.exe to a sequence of commands ending in UPDATE)
		for k in list( kwargs.keys() ):
			if k.upper() == 'DEMAND': finalKwargs.append( ( 'DEMAND!', kwargs.pop( k ) ) )

	commands = [ ' '.join( arg.upper().replace( '=', ' ' ).split() ) for arg in pargs ]
	commands += [ '%s %d' % ( k.upper().replace( '_', '.' ), v ) for k, v in list( kwargs.items() ) + finalKwargs ]
	#if workaround:
	#	sleepMsec = 100
	#	if workaround > 1: sleepMsec = workaround
	#	enable = { parts[ 0 ] : parts[ 1 ] for command in commands for parts in [ command.split() + [ '?' ] ] if command.strip() }.get( 'CONTROL.ENABLE', None )
	#	commands = [ 'CONTROL.ENABLE 0', 'UPDATE', 'SLEEP %d' % sleepMsec ] + commands
	#	if enable is None: commands.append( 'CONTROL.ENABLE 1' )
	#	commands.append( 'UPDATE' )
	prependRead = None
	appendUpdate = None
	for command in commands:
		if ' ' in command or '=' in command:
			if command.startswith( 'DEMAND!' ): appendUpdate = None  # DEMAND! expands inside ds8.exe into a string of commands ending in UPDATE, so no additional UPDATE is necessary
			elif appendUpdate is None: appendUpdate = True
		elif command.upper() == 'READ': prependRead = False
		elif command.upper() == 'UPDATE': appendUpdate = False
		elif prependRead is None: prependRead = True
	if appendUpdate: commands.append( 'UPDATE' )
	elif prependRead: commands.insert( 0, 'READ' )
	commands = '\n'.join( commands ).strip()
	if debug: print( commands )
	if commands: commands += '\n'
	
	sp = subprocess.Popen( [ os.path.join( HERE, 'ds8.exe' ) ], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True )
	out, err = sp.communicate( commands )
	sp.wait()
	#if str is not bytes: out, err = out.decode( 'utf-8' ), err.decode( 'utf-8' )
	if sp.returncode: raise DigitimerException( err )
	output[ : ] = [ line.strip() for line in out.split( '\n' ) if line.strip() ]
	return output  # but remember you'll get nothing back directly unless you're running with threaded=False