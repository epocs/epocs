#include <iostream>
#include <sstream>
//#include "D128RProxy.h"
#include <Windows.h>

#define PROCNAME_SET "DGD128_Set"
typedef int (*PROCTYPE_SET)(
	long int Mode,
	long int Polarity,
	long int Source,
	long int Demand,
	long int PulseWidth,
	long int Dwell,
	long int Recovery,
	long int Enabled);

#define PROCNAME_GET "DGD128_Get"
typedef int (*PROCTYPE_GET)(
	long int * Mode,  
	long int * Polarity,
	long int * Source,
	long int * Demand,
	long int * PulseWidth,
	long int * Dwell,
	long int * Recovery,
	long int * Enabled);

#define PROCNAME_TRIGGER "DGD128_Trigger"
typedef int (*PROCTYPE_TRIGGER)();


#define REPORT( X )   std::cout << #X << " = " << ( X ) << "\n"
#define FAIL( RETURN_VALUE, STUFF )   { std::stringstream _ss; _ss << STUFF; mError = _ss.str(); return RETURN_VALUE; } 

HINSTANCE D128RPROXY_DLL; 


class DS8R
{
	private:
		std::string mError;
		PROCTYPE_SET     mDS8Set;
		PROCTYPE_GET     mDS8Get;
		PROCTYPE_TRIGGER mDS8Trigger;
	
	public:
		 DS8R() : mDS8Set( NULL ), mDS8Get( NULL ), mDS8Trigger( NULL ) { mMode = mPolarity = mSource = mDemand = mPulseWidth = mDwell = mRecovery = mEnabled = -1;}
		~DS8R() {}
		
		long int mMode, mPolarity, mSource, mDemand, mPulseWidth, mDwell, mRecovery, mEnabled;
		
		int LoadAPI( void )
		{
			if( mDS8Set && mDS8Get && mDS8Trigger ) return 1;
			mError = "";
			const char * dllName = "D128RProxy.DLL";
			if( D128RPROXY_DLL == NULL && ( D128RPROXY_DLL = LoadLibraryA( dllName ) ) == NULL ) FAIL( 0, "failed to load " << dllName );
			if( ( mDS8Set     = ( PROCTYPE_SET     )GetProcAddress( D128RPROXY_DLL, PROCNAME_SET     ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_SET     << " from " << dllName );
			if( ( mDS8Get     = ( PROCTYPE_GET     )GetProcAddress( D128RPROXY_DLL, PROCNAME_GET     ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_GET     << " from " << dllName );
			if( ( mDS8Trigger = ( PROCTYPE_TRIGGER )GetProcAddress( D128RPROXY_DLL, PROCNAME_TRIGGER ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_TRIGGER << " from " << dllName );
			return 1;
		}
		int Get( void )
		{
			if( !mDS8Get ) FAIL( 0, PROCNAME_GET << " not loaded" );
			if( !mDS8Get( &mMode, &mPolarity, &mSource, &mDemand, &mPulseWidth, &mDwell, &mRecovery, &mEnabled ) ) FAIL( 0, PROCNAME_GET << "() failed" );
			return 1;
		}
		int Set( void )
		{
			if( !mDS8Set ) FAIL( 0, PROCNAME_SET << " not loaded" );
			if( !mDS8Set( mMode, mPolarity, mSource, mDemand, mPulseWidth, mDwell, mRecovery, mEnabled ) ) FAIL( 0, PROCNAME_SET << "() failed" );
			return 1;
		}
		void Report( void )
		{
			REPORT( mMode );
			REPORT( mPolarity );
			REPORT( mSource );
			REPORT( mDemand );
			REPORT( mPulseWidth );
			REPORT( mDwell );
			REPORT( mRecovery );
			REPORT( mEnabled );
		}
		int PrintError( void )
		{
			if( mError.length() ) std::cerr << mError << std::endl;
			return -1;
		}
		
};

int main( int argc, const char argv[] )
{
	DS8R s;
	if( !s.LoadAPI() ) return s.PrintError();
	if( !s.Get()     ) return s.PrintError();
	s.Report();
	s.mDemand = 98;
	s.mPulseWidth = 20; 
	if( !s.Set()     ) return s.PrintError();
	return 0;
}
