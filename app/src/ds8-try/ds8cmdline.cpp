#include <iostream>
#include "ds8.cpp"

#include <stdio.h>
#include <io.h>
int main( int argc, const char * argv[] )
{
    int deviceIndex = -1, serialNumber = -1;
	int nargs = argc - 1;
	if( nargs >= 1 && std::string( argv[ 1 ] ) != "-" ) { std::stringstream ss( argv[ 1 ] ); ss >> deviceIndex; }    // First optional command-line arg: 0-based index of which stimulator to target
	if( nargs >= 2 && std::string( argv[ 2 ] ) != "-" ) { std::stringstream ss( argv[ 2 ] ); ss >> serialNumber; }   // Second optional command-line arg: serial number to target if index is < 0
	D128Device stimulator( deviceIndex, serialNumber );
	int previousDevice = -1;
	bool console = _isatty( _fileno( stdin ) );
	while( true )
	{
	    if( stimulator.GetSerialNumber() != previousDevice )
		{
			std::cout << "   Serial number: " << stimulator.GetSerialNumber() << std::endl;
		    std::cout << "Firmware version: " << stimulator.GetFirmwareVersionString() << std::endl;
			previousDevice = stimulator.GetSerialNumber();
		}
		if( stimulator.Error() ) { std::cerr << stimulator.Error() << std::endl; if( console ) stimulator.ClearError(); else return -1; }
		std::string cmd;
		std::getline( std::cin, cmd );
		if( std::cin.fail() ) return 0;
		if( cmd == "EXIT" || cmd == "exit" ) break;
		int returnValue = stimulator.ProcessCommand( cmd );
		if( returnValue > 0 ) std::cout << stimulator.GetResult() << std::endl;
	}
	return 0;
}
